package com.zdcp.util.file;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.Properties;

/**
 * 加载文件并读取文件中数据
 * 
 * @author yweijian
 * @date 2019年8月18日
 * @version 1.0
 */
public class ReadFileUtils extends Object {
	
	private static String APPLICATION_RESOURCE = "application.properties";

	private static ClassLoader defaultClassLoader;

	private ReadFileUtils() {
	}

	/**
	 * Returns the default classloader (may be null).
	 * 
	 * @return The default classloader
	 */
	public static ClassLoader getDefaultClassLoader() {
		return defaultClassLoader;
	}

	/**
	 * Sets the default classloader
	 * 
	 * @param defaultClassLoader
	 *            the new default ClassLoader
	 */
	public static void setDefaultClassLoader(ClassLoader defaultClassLoader) {
		ReadFileUtils.defaultClassLoader = defaultClassLoader;
	}

	/**
	 * Returns the URL of the resource on the classpath
	 * 
	 * @param resource
	 *            The resource to find
	 * @return The resource
	 * @throws IOException
	 *             If the resource cannot be found or read
	 */
	public static URL getResourceURL(String resource) throws IOException {
		return getResourceURL(getClassLoader(), resource);
	}

	/**
	 * Returns the URL of the resource on the classpath
	 * 
	 * @param loader
	 *            The classloader used to load the resource
	 * @param resource
	 *            The resource to find
	 * @return The resource
	 * @throws IOException
	 *             If the resource cannot be found or read
	 */
	public static URL getResourceURL(ClassLoader loader, String resource) throws IOException {
		URL url = null;
		if (loader != null)
			url = loader.getResource(resource);
		if (url == null)
			url = ClassLoader.getSystemResource(resource);
		if (url == null)
			throw new IOException("Could not find resource " + resource);
		return url;
	}

	/**
	 * Returns a resource on the classpath as a Stream object
	 * 
	 * @param resource
	 *            The resource to find
	 * @return The resource
	 * @throws IOException
	 *             If the resource cannot be found or read
	 */
	public static InputStream getResourceAsStream(String resource) throws IOException {
		return getResourceAsStream(getClassLoader(), resource);
	}

	/**
	 * Returns a resource on the classpath as a Stream object
	 * 
	 * @param loader
	 *            The classloader used to load the resource
	 * @param resource
	 *            The resource to find
	 * @return The resource
	 * @throws IOException
	 *             If the resource cannot be found or read
	 */
	public static InputStream getResourceAsStream(ClassLoader loader, String resource) throws IOException {
		InputStream in = null;
		if (loader != null)
			in = loader.getResourceAsStream(resource);
		if (in == null)
			in = ClassLoader.getSystemResourceAsStream(resource);
		if (in == null)
			throw new IOException("Could not find resource " + resource);
		return in;
	}

	/**
	 * Returns a resource on the classpath as a Properties object
	 * 
	 * @param resource
	 *            The resource to find
	 * @return The resource
	 * @throws IOException
	 *             If the resource cannot be found or read
	 */
	public static Properties getResourceAsProperties(String resource) throws IOException {
		Properties props = new Properties();
		InputStream in = null;
		String propfile = resource;
		in = getResourceAsStream(propfile);
		props.load(in);
		in.close();
		return props;
	}

	/**
	 * Returns a resource on the classpath as a Properties object
	 * 
	 * @param loader
	 *            The classloader used to load the resource
	 * @param resource
	 *            The resource to find
	 * @return The resource
	 * @throws IOException
	 *             If the resource cannot be found or read
	 */
	public static Properties getResourceAsProperties(ClassLoader loader, String resource) throws IOException {
		Properties props = new Properties();
		InputStream in = null;
		String propfile = resource;
		in = getResourceAsStream(loader, propfile);
		props.load(in);
		in.close();
		return props;
	}

	/**
	 * Returns a resource on the classpath as a Reader object
	 * 
	 * @param resource
	 *            The resource to find
	 * @return The resource
	 * @throws IOException
	 *             If the resource cannot be found or read
	 */
	public static Reader getResourceAsReader(String resource) throws IOException {
		return new InputStreamReader(getResourceAsStream(resource));
	}

	/**
	 * Returns a resource on the classpath as a Reader object
	 * 
	 * @param loader
	 *            The classloader used to load the resource
	 * @param resource
	 *            The resource to find
	 * @return The resource
	 * @throws IOException
	 *             If the resource cannot be found or read
	 */
	public static Reader getResourceAsReader(ClassLoader loader, String resource) throws IOException {
		return new InputStreamReader(getResourceAsStream(loader, resource));
	}

	/**
	 * Returns a resource on the classpath as a File object
	 * 
	 * @param resource
	 *            The resource to find
	 * @return The resource
	 * @throws IOException
	 *             If the resource cannot be found or read
	 */
	public static File getResourceAsFile(String resource) throws IOException {
		return new File(getResourceURL(resource).getFile());
	}

	/**
	 * Returns a resource on the classpath as a File object
	 * 
	 * @param loader
	 *            - the classloader used to load the resource
	 * @param resource
	 *            - the resource to find
	 * @return The resource
	 * @throws IOException
	 *             If the resource cannot be found or read
	 */
	public static File getResourceAsFile(ClassLoader loader, String resource) throws IOException {
		return new File(getResourceURL(loader, resource).getFile());
	}

	/**
	 * Gets a URL as an input stream
	 * 
	 * @param urlString
	 *            - the URL to get
	 * @return An input stream with the data from the URL
	 * @throws IOException
	 *             If the resource cannot be found or read
	 */
	public static InputStream getUrlAsStream(String urlString) throws IOException {
		URL url = new URL(urlString);
		URLConnection conn = url.openConnection();
		return conn.getInputStream();
	}

	/**
	 * Gets a URL as a Reader
	 * 
	 * @param urlString
	 *            - the URL to get
	 * @return A Reader with the data from the URL
	 * @throws IOException
	 *             If the resource cannot be found or read
	 */
	public static Reader getUrlAsReader(String urlString) throws IOException {
		return new InputStreamReader(getUrlAsStream(urlString));
	}

	/**
	 * Gets a URL as a Properties object
	 * 
	 * @param urlString
	 *            - the URL to get
	 * @return A Properties object with the data from the URL
	 * @throws IOException
	 *             If the resource cannot be found or read
	 */
	public static Properties getUrlAsProperties(String urlString) throws IOException {
		Properties props = new Properties();
		InputStream in = null;
		String propfile = urlString;
		in = getUrlAsStream(propfile);
		props.load(in);
		in.close();
		return props;
	}

	/**
	 * Loads a class
	 * 
	 * @param className
	 *            - the class to load
	 * @return The loaded class
	 * @throws ClassNotFoundException
	 *             If the class cannot be found (duh!)
	 */

	public static Class<?> classForName(String className) throws ClassNotFoundException {
		Class<?> clazz = null;
		try {
			clazz = getClassLoader().loadClass(className);
		} catch (Exception e) {
			// Ignore. Failsafe below.
		}
		if (clazz == null) {
			clazz = Class.forName(className);
		}
		return clazz;
	}

	/**
	 * Creates an instance of a class
	 * 
	 * @param className
	 *            - the class to create
	 * @return An instance of the class
	 * @throws ClassNotFoundException
	 *             If the class cannot be found (duh!)
	 * @throws InstantiationException
	 *             If the class cannot be instantiaed
	 * @throws IllegalAccessException
	 *             If the class is not public, or other access problems arise
	 */
	public static Object instantiate(String className)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		return instantiate(classForName(className));
	}

	/**
	 * Creates an instance of a class
	 * 
	 * @param clazz
	 *            - the class to create
	 * @return An instance of the class
	 * @throws InstantiationException
	 *             If the class cannot be instantiaed
	 * @throws IllegalAccessException
	 *             If the class is not public, or other access problems arise
	 */
	public static Object instantiate(Class<?> clazz) throws InstantiationException, IllegalAccessException {
		return clazz.newInstance();
	}

	/**
	 * 
	 * @return
	 */
	private static ClassLoader getClassLoader() {
		if (defaultClassLoader != null) {
			return defaultClassLoader;
		} else {
			return Thread.currentThread().getContextClassLoader();
		}
	}

	/**
	 * 获取资源文件的key对应的内容
	 * 
	 * @param key
	 *            资源文件的key
	 * @return 返回 资源文件中key对应的值
	 * 
	 */
	public static String getValue(String resource, String key) {
		Properties p;
		String htmlstr = "";
		String encoding = "utf-8";
		try {
			p = ReadFileUtils.getResourceAsProperties(resource);
			htmlstr = new String(p.getProperty(key).getBytes("iso-8859-1"), encoding);
		} catch (IOException e) {
			htmlstr = e.getMessage();
		}
		return htmlstr;
	}
	
	/**
	 * 获取资源文件的key对应的内容,默认文件application.properties
	 * @param key
	 * @return
	 */
	public static String getValue(String key) {
		Properties p;
		String htmlstr = "";
		String encoding = "utf-8";
		try {
			p = ReadFileUtils.getResourceAsProperties(APPLICATION_RESOURCE);
			htmlstr = new String(p.getProperty(key).getBytes("iso-8859-1"), encoding);
		} catch (IOException e) {
			htmlstr = e.getMessage();
		}
		return htmlstr;
	}

}