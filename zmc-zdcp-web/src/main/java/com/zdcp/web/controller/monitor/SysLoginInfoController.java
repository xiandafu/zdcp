package com.zdcp.web.controller.monitor;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zdcp.core.annotation.Log;
import com.zdcp.core.bean.JsonResult;
import com.zdcp.core.bean.TableInfo;
import com.zdcp.core.enums.BusinessType;
import com.zdcp.core.web.controller.BaseController;
import com.zdcp.core.web.util.ExcelUtils;
import com.zdcp.system.bean.SysLoginInfo;
import com.zdcp.system.service.ISysLoginInfoService;
import com.zdcp.util.text.Convert;

/**
 * 系统访问记录
 * 
 * @author yweijian
 * @date 2019年8月23日
 * @version 1.0
 */
@Controller
@RequestMapping("/monitor/logininfo")
public class SysLoginInfoController extends BaseController {
	private String prefix = "monitor/logininfo";

	@Autowired
	private ISysLoginInfoService loginInfoService;

	@RequiresPermissions("monitor:logininfo:view")
	@GetMapping()
	public String logininfo() {
		return prefix + "/logininfo";
	}

	@SuppressWarnings("unchecked")
	@RequiresPermissions("monitor:logininfo:list")
	@PostMapping("/list")
	@ResponseBody
	public TableInfo list(SysLoginInfo loginInfo) {
		initPageQuery(loginInfo);
		loginInfoService.selectLoginInfoList(query);
		return getDataTable(query);
	}

	@Log(title = "登陆日志", businessType = BusinessType.EXPORT)
	@RequiresPermissions("monitor:logininfo:export")
	@PostMapping("/export")
	@ResponseBody
	public JsonResult export(SysLoginInfo loginInfo) {
		List<SysLoginInfo> list = loginInfoService.selectLoginInfoList(loginInfo);
		ExcelUtils<SysLoginInfo> util = new ExcelUtils<SysLoginInfo>(SysLoginInfo.class);
		return util.exportExcel(list, "登陆日志");
	}

	@RequiresPermissions("monitor:logininfo:remove")
	@Log(title = "登陆日志", businessType = BusinessType.DELETE)
	@PostMapping("/remove")
	@ResponseBody
	public JsonResult remove(String ids) {
		loginInfoService.deleteLoginInfoByIds(Convert.toStrArray(ids));
		return JsonResult.success();
	}

	@RequiresPermissions("monitor:logininfo:remove")
	@Log(title = "登陆日志", businessType = BusinessType.CLEAN)
	@PostMapping("/clean")
	@ResponseBody
	public JsonResult clean() {
		loginInfoService.cleanLoginInfo();
		return JsonResult.success();
	}
}
