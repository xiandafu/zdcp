package com.zdcp.web.controller.monitor;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zdcp.core.annotation.Log;
import com.zdcp.core.bean.JsonResult;
import com.zdcp.core.bean.TableInfo;
import com.zdcp.core.enums.BusinessType;
import com.zdcp.core.web.controller.BaseController;
import com.zdcp.core.web.util.ExcelUtils;
import com.zdcp.system.bean.SysOperLog;
import com.zdcp.system.service.ISysOperLogService;
import com.zdcp.util.text.Convert;

/**
 * 操作日志记录
 * 
 * @author yweijian
 * @date 2019年8月23日
 * @version 1.0
 */
@Controller
@RequestMapping("/monitor/operlog")
public class SysOperlogController extends BaseController {
	private String prefix = "monitor/operlog";

	@Autowired
	private ISysOperLogService operLogService;

	@RequiresPermissions("monitor:operlog:view")
	@GetMapping()
	public String operlog() {
		return prefix + "/operlog";
	}

	@SuppressWarnings("unchecked")
	@RequiresPermissions("monitor:operlog:list")
	@PostMapping("/list")
	@ResponseBody
	public TableInfo list(SysOperLog operLog) {
		initPageQuery(operLog);
		operLogService.selectOperLogList(query);
		return getDataTable(query);
	}

	@Log(title = "操作日志", businessType = BusinessType.EXPORT)
	@RequiresPermissions("monitor:operlog:export")
	@PostMapping("/export")
	@ResponseBody
	public JsonResult export(SysOperLog operLog) {
		List<SysOperLog> list = operLogService.selectOperLogList(operLog);
		ExcelUtils<SysOperLog> util = new ExcelUtils<SysOperLog>(SysOperLog.class);
		return util.exportExcel(list, "操作日志");
	}

	@RequiresPermissions("monitor:operlog:remove")
	@PostMapping("/remove")
	@ResponseBody
	public JsonResult remove(String ids) {
		operLogService.deleteOperLogByIds(Convert.toStrArray(ids));
		return JsonResult.success();
	}

	@RequiresPermissions("monitor:operlog:detail")
	@GetMapping("/detail/{operId}")
	public String detail(@PathVariable("operId") Long operId, ModelMap mmap) {
		mmap.put("operLog", operLogService.selectOperLogById(operId));
		return prefix + "/detail";
	}

	@Log(title = "操作日志", businessType = BusinessType.CLEAN)
	@RequiresPermissions("monitor:operlog:remove")
	@PostMapping("/clean")
	@ResponseBody
	public JsonResult clean() {
		operLogService.cleanOperLog();
		return JsonResult.success();
	}
}
