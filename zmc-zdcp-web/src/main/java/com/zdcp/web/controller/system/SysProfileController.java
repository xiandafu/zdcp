package com.zdcp.web.controller.system;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.zdcp.core.annotation.Log;
import com.zdcp.core.bean.JsonResult;
import com.zdcp.core.enums.BusinessType;
import com.zdcp.core.web.controller.BaseController;
import com.zdcp.system.bean.SysUser;
import com.zdcp.system.service.ISysUserService;
import com.zdcp.system.shiro.service.SysLoginService;
import com.zdcp.system.shiro.util.ShiroUtils;
import com.zdcp.util.StringUtils;
import com.zdcp.util.file.FileUploadUtils;

/**
 * 个人信息控制层
 * 
 * @author yweijian
 * @date 2019年8月18日
 * @version 1.0
 */
@Controller
@RequestMapping("/system/user/profile")
public class SysProfileController extends BaseController {
	private static final Logger log = LoggerFactory.getLogger(SysProfileController.class);

	private String prefix = "system/user/profile";

	@Autowired
	private ISysUserService userService;

	//@Autowired
	private SysLoginService loginService;

	/**
	 * 个人信息
	 */
	@GetMapping()
	public String profile(ModelMap modelMap) {
		SysUser user = ShiroUtils.getSysUser();
		modelMap.put("user", user);
		modelMap.put("roleGroup", userService.selectUserRoleGroup(user.getUserId()));
		modelMap.put("postGroup", userService.selectUserPostGroup(user.getUserId()));
		return prefix + "/profile";
	}

	@GetMapping("/checkPassword")
	@ResponseBody
	public boolean checkPassword(String password) {
		SysUser user = ShiroUtils.getSysUser();
		if (loginService.matches(user, password)) {
			return true;
		}
		return false;
	}

	@GetMapping("/resetPwd")
	public String resetPwd(ModelMap modelMap) {
		SysUser user = ShiroUtils.getSysUser();
		modelMap.put("user", userService.selectUserById(user.getUserId()));
		return prefix + "/resetPwd";
	}

	@Log(title = "重置密码", businessType = BusinessType.UPDATE)
	@PostMapping("/resetPwd")
	@ResponseBody
	public JsonResult resetPwd(String oldPassword, String newPassword) {
		SysUser user = ShiroUtils.getSysUser();
		if (StringUtils.isNotEmpty(newPassword) && loginService.matches(user, oldPassword)) {
			user.setSalt(ShiroUtils.randomSalt());
			user.setPassword(loginService.encryptPassword(user.getLoginName(), newPassword, user.getSalt()));
			if (userService.resetUserPwd(user) > 0) {
				ShiroUtils.setSysUser(userService.selectUserById(user.getUserId()));
				return JsonResult.success();
			}
			return JsonResult.error();
		} else {
			return JsonResult.error("修改密码失败，旧密码错误");
		}
	}

	/**
	 * 修改用户
	 */
	@GetMapping("/edit")
	public String edit(ModelMap modelMap) {
		SysUser user = ShiroUtils.getSysUser();
		modelMap.put("user", userService.selectUserById(user.getUserId()));
		return prefix + "/edit";
	}

	/**
	 * 修改头像
	 */
	@GetMapping("/avatar")
	public String avatar(ModelMap modelMap) {
		SysUser user = ShiroUtils.getSysUser();
		modelMap.put("user", userService.selectUserById(user.getUserId()));
		return prefix + "/avatar";
	}

	/**
	 * 修改用户
	 */
	@Log(title = "个人信息", businessType = BusinessType.UPDATE)
	@PostMapping("/update")
	@ResponseBody
	public JsonResult update(SysUser user) {
		SysUser currentUser = ShiroUtils.getSysUser();
		currentUser.setUserName(user.getUserName());
		currentUser.setEmail(user.getEmail());
		currentUser.setPhonenumber(user.getPhonenumber());
		currentUser.setSex(user.getSex());
		if (userService.updateUserInfo(currentUser) > 0) {
			ShiroUtils.setSysUser(userService.selectUserById(currentUser.getUserId()));
			return JsonResult.success();
		}
		return JsonResult.error();
	}

	/**
	 * 保存头像
	 */
	@Log(title = "个人信息", businessType = BusinessType.UPDATE)
	@PostMapping("/updateAvatar")
	@ResponseBody
	public JsonResult updateAvatar(@RequestParam("avatarfile") MultipartFile file) {
		SysUser currentUser = ShiroUtils.getSysUser();
		try {
			if (!file.isEmpty()) {
				String avatar = FileUploadUtils.upload(file);
				currentUser.setAvatar(avatar);
				if (userService.updateUserInfo(currentUser) > 0) {
					ShiroUtils.setSysUser(userService.selectUserById(currentUser.getUserId()));
					return JsonResult.success();
				}
			}
			return JsonResult.error();
		} catch (Exception e) {
			log.error("修改头像失败！", e);
			return JsonResult.error(e.getMessage());
		}
	}
}
