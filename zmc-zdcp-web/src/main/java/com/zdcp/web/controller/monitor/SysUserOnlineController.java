package com.zdcp.web.controller.monitor;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zdcp.core.annotation.Log;
import com.zdcp.core.bean.JsonResult;
import com.zdcp.core.bean.TableInfo;
import com.zdcp.core.enums.BusinessType;
import com.zdcp.core.enums.OnlineStatus;
import com.zdcp.core.web.controller.BaseController;
import com.zdcp.system.bean.SysUserOnline;
import com.zdcp.system.service.ISysUserOnlineService;
import com.zdcp.system.shiro.session.OnlineSession;
import com.zdcp.system.shiro.session.OnlineSessionDao;
import com.zdcp.system.shiro.util.ShiroUtils;

/**
 * 在线用户监控
 * 
 * @author yweijian
 * @date 2019年8月23日
 * @version 1.0
 */
@Controller
@RequestMapping("/monitor/online")
public class SysUserOnlineController extends BaseController {
	private String prefix = "monitor/online";

	@Autowired
	private ISysUserOnlineService userOnlineService;

	@Autowired
	private OnlineSessionDao onlineSessionDao;

	@RequiresPermissions("monitor:online:view")
	@GetMapping()
	public String online() {
		return prefix + "/online";
	}

	@SuppressWarnings("unchecked")
	@RequiresPermissions("monitor:online:list")
	@PostMapping("/list")
	@ResponseBody
	public TableInfo list(SysUserOnline userOnline) {
		initPageQuery(userOnline);
		userOnlineService.selectUserOnlineList(query);
		return getDataTable(query);
	}

	@RequiresPermissions("monitor:online:batchForceLogout")
	@Log(title = "在线用户", businessType = BusinessType.FORCE)
	@PostMapping("/batchForceLogout")
	@ResponseBody
	public JsonResult batchForceLogout(@RequestParam("ids[]") String[] ids) {
		for (String sessionId : ids) {
			SysUserOnline online = userOnlineService.selectOnlineById(sessionId);
			if (online == null) {
				return JsonResult.error("用户已下线");
			}
			OnlineSession onlineSession = (OnlineSession) onlineSessionDao.readSession(online.getSessionId());
			if (onlineSession == null) {
				return JsonResult.error("用户已下线");
			}
			if (sessionId.equals(ShiroUtils.getSessionId())) {
				return JsonResult.error("当前登陆用户无法强退");
			}
			onlineSession.setStatus(OnlineStatus.OFF_LINE);
			onlineSessionDao.update(onlineSession);
			online.setStatus(OnlineStatus.OFF_LINE);
			userOnlineService.insertOnline(online);
		}
		return JsonResult.success();
	}

	@RequiresPermissions("monitor:online:forceLogout")
	@Log(title = "在线用户", businessType = BusinessType.FORCE)
	@PostMapping("/forceLogout")
	@ResponseBody
	public JsonResult forceLogout(String sessionId) {
		SysUserOnline online = userOnlineService.selectOnlineById(sessionId);
		if (sessionId.equals(ShiroUtils.getSessionId())) {
			return JsonResult.error("当前登陆用户无法强退");
		}
		if (online == null) {
			return JsonResult.error("用户已下线");
		}
		OnlineSession onlineSession = (OnlineSession) onlineSessionDao.readSession(online.getSessionId());
		if (onlineSession == null) {
			return JsonResult.error("用户已下线");
		}
		onlineSession.setStatus(OnlineStatus.OFF_LINE);
		onlineSessionDao.update(onlineSession);
		online.setStatus(OnlineStatus.OFF_LINE);
		userOnlineService.updateOnline(online);
		return JsonResult.success();
	}
}
