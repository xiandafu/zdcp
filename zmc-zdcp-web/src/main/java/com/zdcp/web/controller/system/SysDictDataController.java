package com.zdcp.web.controller.system;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zdcp.core.annotation.FormToken;
import com.zdcp.core.annotation.Log;
import com.zdcp.core.bean.JsonResult;
import com.zdcp.core.bean.TableInfo;
import com.zdcp.core.enums.BusinessType;
import com.zdcp.core.enums.TokenType;
import com.zdcp.core.web.controller.BaseController;
import com.zdcp.core.web.util.ExcelUtils;
import com.zdcp.system.bean.SysDictData;
import com.zdcp.system.service.ISysDictDataService;
import com.zdcp.system.shiro.util.ShiroUtils;
import com.zdcp.util.text.Convert;

/**
 * 数据字典信息控制器
 * @author yweijian
 * @date 2019年8月23日
 * @version 1.0
 */
@Controller
@RequestMapping("/system/dict/data")
public class SysDictDataController extends BaseController {
	private String prefix = "system/dict/data";

	@Autowired
	private ISysDictDataService dictDataService;

	@RequiresPermissions("system:dict:view")
	@GetMapping()
	public String dictData() {
		return prefix + "/data";
	}

	@SuppressWarnings("unchecked")
	@PostMapping("/list")
	@RequiresPermissions("system:dict:list")
	@ResponseBody
	public TableInfo list(SysDictData dictData) {
		initPageQuery(dictData);
		dictDataService.selectDictDataList(query);
		return getDataTable(query);
	}

	@Log(title = "字典数据", businessType = BusinessType.EXPORT)
	@RequiresPermissions("system:dict:export")
	@PostMapping("/export")
	@ResponseBody
	public JsonResult export(SysDictData dictData) {
		List<SysDictData> list = dictDataService.selectDictDataList(dictData);
		ExcelUtils<SysDictData> util = new ExcelUtils<SysDictData>(SysDictData.class);
		return util.exportExcel(list, "字典数据");
	}

	/**
	 * 新增字典类型
	 */
	@FormToken
	@GetMapping("/add/{dictType}")
	public String add(@PathVariable("dictType") String dictType, ModelMap mmap) {
		mmap.put("dictType", dictType);
		return prefix + "/add";
	}

	/**
	 * 新增保存字典类型
	 */
	@FormToken(TokenType.DESTROY)
	@Log(title = "字典数据", businessType = BusinessType.INSERT)
	@RequiresPermissions("system:dict:add")
	@PostMapping("/add")
	@ResponseBody
	public JsonResult addSave(@Validated SysDictData dict) {
		dict.setCreateBy(ShiroUtils.getLoginName());
		dictDataService.insertDictData(dict);
		return JsonResult.success();
	}

	/**
	 * 修改字典类型
	 */
	@GetMapping("/edit/{dictCode}")
	public String edit(@PathVariable("dictCode") Long dictCode, ModelMap mmap) {
		mmap.put("dict", dictDataService.selectDictDataById(dictCode));
		return prefix + "/edit";
	}

	/**
	 * 修改保存字典类型
	 */
	@Log(title = "字典数据", businessType = BusinessType.UPDATE)
	@RequiresPermissions("system:dict:edit")
	@PostMapping("/edit")
	@ResponseBody
	public JsonResult editSave(@Validated SysDictData dict) {
		dict.setUpdateBy(ShiroUtils.getLoginName());
		dictDataService.updateDictData(dict);
		return JsonResult.success();
	}

	@Log(title = "字典数据", businessType = BusinessType.DELETE)
	@RequiresPermissions("system:dict:remove")
	@PostMapping("/remove")
	@ResponseBody
	public JsonResult remove(String ids) {
		dictDataService.deleteDictDataByIds(Convert.toStrArray(ids));
		return JsonResult.success();
	}
}
