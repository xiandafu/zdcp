package com.zdcp.web.controller.system;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zdcp.core.annotation.FormToken;
import com.zdcp.core.annotation.Log;
import com.zdcp.core.bean.JsonResult;
import com.zdcp.core.bean.TableInfo;
import com.zdcp.core.enums.BusinessType;
import com.zdcp.core.enums.TokenType;
import com.zdcp.core.web.controller.BaseController;
import com.zdcp.core.web.util.ExcelUtils;
import com.zdcp.system.bean.SysPost;
import com.zdcp.system.constant.SysConstants;
import com.zdcp.system.service.ISysPostService;
import com.zdcp.system.shiro.util.ShiroUtils;
import com.zdcp.util.text.Convert;

/**
 *  岗位信息控制层
 * @author yweijian
 * @date 2019年8月18日
 * @version 1.0
 */
@Controller
@RequestMapping("/system/post")
public class SysPostController extends BaseController {
	private String prefix = "system/post";

	@Autowired
	private ISysPostService postService;

	@RequiresPermissions("system:post:view")
	@GetMapping()
	public String operlog() {
		return prefix + "/post";
	}

	@SuppressWarnings("unchecked")
	@RequiresPermissions("system:post:list")
	@PostMapping("/list")
	@ResponseBody
	public TableInfo list(SysPost post) {
		initPageQuery(post);
		postService.selectPostList(query);
		return getDataTable(query);
	}

	@Log(title = "岗位管理", businessType = BusinessType.EXPORT)
	@RequiresPermissions("system:post:export")
	@PostMapping("/export")
	@ResponseBody
	public JsonResult export(SysPost post) {
		List<SysPost> list = postService.selectPostList(post);
		ExcelUtils<SysPost> util = new ExcelUtils<SysPost>(SysPost.class);
		return util.exportExcel(list, "岗位数据");
	}

	@RequiresPermissions("system:post:remove")
	@Log(title = "岗位管理", businessType = BusinessType.DELETE)
	@PostMapping("/remove")
	@ResponseBody
	public JsonResult remove(String ids) {
		try {
			postService.deletePostByIds(Convert.toStrArray(ids));
			return JsonResult.success();
		} catch (Exception e) {
			return JsonResult.error(e.getMessage());
		}
	}

	/**
	 * 新增岗位
	 */
	@FormToken
	@GetMapping("/add")
	public String add() {
		return prefix + "/add";
	}

	/**
	 * 新增保存岗位
	 */
	@FormToken(TokenType.DESTROY)
	@RequiresPermissions("system:post:add")
	@Log(title = "岗位管理", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public JsonResult addSave(@Validated SysPost post) {
		if (SysConstants.NOT_UNIQUE.equals(postService.checkPostNameUnique(post))) {
			return JsonResult.error("新增岗位'" + post.getPostName() + "'失败，岗位名称已存在");
		} else if (SysConstants.NOT_UNIQUE.equals(postService.checkPostCodeUnique(post))) {
			return JsonResult.error("新增岗位'" + post.getPostName() + "'失败，岗位编码已存在");
		}
		post.setCreateBy(ShiroUtils.getLoginName());
		postService.insertPost(post);
		return JsonResult.success();
	}

	/**
	 * 修改岗位
	 */
	@GetMapping("/edit/{postId}")
	public String edit(@PathVariable("postId") Long postId, ModelMap mmap) {
		mmap.put("post", postService.selectPostById(postId));
		return prefix + "/edit";
	}

	/**
	 * 修改保存岗位
	 */
	@RequiresPermissions("system:post:edit")
	@Log(title = "岗位管理", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public JsonResult editSave(@Validated SysPost post) {
		if (SysConstants.NOT_UNIQUE.equals(postService.checkPostNameUnique(post))) {
			return JsonResult.error("修改岗位'" + post.getPostName() + "'失败，岗位名称已存在");
		} else if (SysConstants.NOT_UNIQUE.equals(postService.checkPostCodeUnique(post))) {
			return JsonResult.error("修改岗位'" + post.getPostName() + "'失败，岗位编码已存在");
		}
		post.setUpdateBy(ShiroUtils.getLoginName());
		postService.updatePost(post);
		return JsonResult.success();
	}

	/**
	 * 校验岗位名称
	 */
	@PostMapping("/checkPostNameUnique")
	@ResponseBody
	public String checkPostNameUnique(SysPost post) {
		return postService.checkPostNameUnique(post);
	}

	/**
	 * 校验岗位编码
	 */
	@PostMapping("/checkPostCodeUnique")
	@ResponseBody
	public String checkPostCodeUnique(SysPost post) {
		return postService.checkPostCodeUnique(post);
	}
}
