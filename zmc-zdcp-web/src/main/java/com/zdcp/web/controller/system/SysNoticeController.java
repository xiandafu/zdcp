package com.zdcp.web.controller.system;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zdcp.core.annotation.FormToken;
import com.zdcp.core.annotation.Log;
import com.zdcp.core.bean.JsonResult;
import com.zdcp.core.bean.TableInfo;
import com.zdcp.core.enums.BusinessType;
import com.zdcp.core.enums.TokenType;
import com.zdcp.core.web.controller.BaseController;
import com.zdcp.system.bean.SysNotice;
import com.zdcp.system.service.ISysNoticeService;
import com.zdcp.system.shiro.util.ShiroUtils;
import com.zdcp.util.text.Convert;

/**
 * 公告 信息控制层
 * 
 * @author yweijian
 * @date 2019年8月18日
 * @version 1.0
 */
@Controller
@RequestMapping("/system/notice")
public class SysNoticeController extends BaseController {
	private String prefix = "system/notice";

	@Autowired
	private ISysNoticeService noticeService;

	@RequiresPermissions("system:notice:view")
	@GetMapping()
	public String notice() {
		return prefix + "/notice";
	}

	/**
	 * 查询公告列表
	 */
	@SuppressWarnings("unchecked")
	@RequiresPermissions("system:notice:list")
	@PostMapping("/list")
	@ResponseBody
	public TableInfo list(SysNotice notice) {
		initPageQuery(notice);
		noticeService.selectNoticeList(query);
		return getDataTable(query);
	}

	/**
	 * 新增公告
	 */
	@FormToken
	@GetMapping("/add")
	public String add() {
		return prefix + "/add";
	}

	/**
	 * 新增保存公告
	 */
	@FormToken(TokenType.DESTROY)
	@RequiresPermissions("system:notice:add")
	@Log(title = "通知公告", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public JsonResult addSave(SysNotice notice) {
		notice.setCreateBy(ShiroUtils.getLoginName());
		noticeService.insertNotice(notice);
		return JsonResult.success();
	}

	/**
	 * 修改公告
	 */
	@GetMapping("/edit/{noticeId}")
	public String edit(@PathVariable("noticeId") Long noticeId, ModelMap mmap) {
		mmap.put("notice", noticeService.selectNoticeById(noticeId));
		return prefix + "/edit";
	}

	/**
	 * 修改保存公告
	 */
	@RequiresPermissions("system:notice:edit")
	@Log(title = "通知公告", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public JsonResult editSave(SysNotice notice) {
		notice.setUpdateBy(ShiroUtils.getLoginName());
		noticeService.updateNotice(notice);
		return JsonResult.success();
	}

	/**
	 * 删除公告
	 */
	@RequiresPermissions("system:notice:remove")
	@Log(title = "通知公告", businessType = BusinessType.DELETE)
	@PostMapping("/remove")
	@ResponseBody
	public JsonResult remove(String ids) {
		noticeService.deleteNoticeByIds(Convert.toStrArray(ids));
		return JsonResult.success();
	}
}
