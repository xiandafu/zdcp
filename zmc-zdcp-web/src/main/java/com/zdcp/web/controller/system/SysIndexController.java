package com.zdcp.web.controller.system;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import com.zdcp.core.web.controller.BaseController;
import com.zdcp.system.bean.SysMenu;
import com.zdcp.system.bean.SysUser;
import com.zdcp.system.service.ISysMenuService;
import com.zdcp.system.shiro.util.ShiroUtils;

/**
 * 首页控制层
 * @author yweijian
 * @date 2019年8月18日
 * @version 1.0
 */
@Controller
public class SysIndexController extends BaseController {
	@Autowired
	private ISysMenuService menuService;

	/**
	 * 系统首页
	 * @param mmap
	 * @return
	 */
	@GetMapping("/index")
	public String index(ModelMap mmap) {
		// 取身份信息
		SysUser user = ShiroUtils.getSysUser();
		// 根据用户id取出菜单
		List<SysMenu> menus = menuService.selectMenusByUser(user);
		mmap.put("menus", menus);
		mmap.put("user", user);
		return "index";
	}

	/**
	 * 系统简介
	 * @param mmap
	 * @return
	 */
	@GetMapping("/system/main")
	public String main(ModelMap mmap) {
		return "main";
	}
}
