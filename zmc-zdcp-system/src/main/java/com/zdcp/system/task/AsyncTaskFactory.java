package com.zdcp.system.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;

import com.zdcp.core.constant.Constants;
import com.zdcp.system.bean.SysLoginInfo;
import com.zdcp.system.bean.SysOperLog;
import com.zdcp.system.bean.SysUserOnline;
import com.zdcp.system.service.ISysOperLogService;
import com.zdcp.system.service.ISysUserOnlineService;
import com.zdcp.system.service.impl.SysLoginInfoServiceImpl;
import com.zdcp.system.shiro.session.OnlineSession;
import com.zdcp.system.shiro.util.LogUtils;
import com.zdcp.system.shiro.util.ShiroUtils;
import com.zdcp.util.AddressUtils;
import com.zdcp.util.ServletUtils;
import com.zdcp.util.spring.SpringUtils;

import eu.bitwalker.useragentutils.UserAgent;

/**
 * 记录用户行为任务工厂
 * 
 * @author yweijian
 * @date 2019年8月18日
 * @version 1.0
 */
public class AsyncTaskFactory {

	private static final Logger sys_user_logger = LoggerFactory.getLogger("sys-user");

	/**
	 * 同步session到数据库
	 * 
	 * @param session
	 *            在线用户会话
	 */
	@Async
	public static void recordOnlineSession(OnlineSession session) {
		ISysUserOnlineService onlineService = SpringUtils.getBean(ISysUserOnlineService.class);
		SysUserOnline online = onlineService.selectOnlineById(String.valueOf(session.getId()));
		if(online == null){
			online = new SysUserOnline();
			online.setSessionId(String.valueOf(session.getId()));
			online.setDeptName(session.getDeptName());
			online.setLoginName(session.getLoginName());
			online.setStartTimestamp(session.getStartTimestamp());
			online.setLastAccessTime(session.getLastAccessTime());
			online.setExpireTime(session.getTimeout());
			online.setIpaddr(session.getHost());
			online.setLoginLocation(AddressUtils.getRealAddressByIP(session.getHost()));
			online.setBrowser(session.getBrowser());
			online.setOs(session.getOs());
			online.setStatus(session.getStatus());
			onlineService.insertOnline(online);
		}else{
			online.setStatus(session.getStatus());
			onlineService.updateOnline(online);
		}
	}

	/**
	 * 操作日志记录
	 * 
	 * @param operLog
	 *            操作日志信息
	 */
	@Async
	public static void recordOperLog(SysOperLog operLog) {
		// 远程查询操作地点
		operLog.setOperLocation(AddressUtils.getRealAddressByIP(operLog.getOperIp()));
		SpringUtils.getBean(ISysOperLogService.class).insertOperlog(operLog);
	}

	/**
	 * 记录登陆信息
	 * 
	 * @param username
	 *            用户名
	 * @param status
	 *            状态
	 * @param message
	 *            消息
	 * @param args
	 *            列表
	 */
	@Async
	public static void recordLoginInfo(String username, String status, String message, Object... args) {
		String ip = ShiroUtils.getIp();
		UserAgent userAgent = UserAgent.parseUserAgentString(ServletUtils.getRequest().getHeader("User-Agent"));
		String address = AddressUtils.getRealAddressByIP(ip);
		StringBuilder sb = new StringBuilder();
		sb.append(LogUtils.getBlock(ip));
		sb.append(address);
		sb.append(LogUtils.getBlock(username));
		sb.append(LogUtils.getBlock(status));
		sb.append(LogUtils.getBlock(message));
		// 打印信息到日志
		sys_user_logger.info(sb.toString(), args);
		// 获取客户端操作系统
		String os = userAgent.getOperatingSystem().getName();
		// 获取客户端浏览器
		String browser = userAgent.getBrowser().getName();
		// 封装对象
		SysLoginInfo loginInfo = new SysLoginInfo();
		loginInfo.setLoginName(username);
		loginInfo.setIpaddr(ip);
		loginInfo.setLoginLocation(address);
		loginInfo.setBrowser(browser);
		loginInfo.setOs(os);
		loginInfo.setMsg(message);
		// 日志状态
		if (Constants.LOGIN_SUCCESS.equals(status) || Constants.LOGOUT.equals(status)) {
			loginInfo.setStatus(Constants.SUCCESS);
		} else if (Constants.LOGIN_FAIL.equals(status)) {
			loginInfo.setStatus(Constants.FAIL);
		}
		// 插入数据
		SpringUtils.getBean(SysLoginInfoServiceImpl.class).insertLoginInfo(loginInfo);
	}
}
