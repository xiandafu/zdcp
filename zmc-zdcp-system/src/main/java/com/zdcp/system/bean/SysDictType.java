package com.zdcp.system.bean;

import javax.validation.constraints.*;

import org.beetl.sql.core.annotatoin.AutoID;

import com.zdcp.core.annotation.Excel;
import com.zdcp.core.annotation.Excel.ColumnType;
import com.zdcp.core.bean.BaseEntity;

/**
 * 字典类型表
 * 
 * @author yweijian
 * @date 2019年8月16日
 * @version 1.0
 */
public class SysDictType extends BaseEntity {
	private static final long serialVersionUID = 1L;

	/** 字典主键 */
	@AutoID
	@Excel(name = "字典主键", cellType = ColumnType.NUMERIC)
	private Long dictId;

	/** 字典名称 */
	@Excel(name = "字典名称")
	private String dictName;

	/** 字典类型 */
	@Excel(name = "字典类型 ")
	private String dictType;

	/** 状态（0正常 1停用） */
	@Excel(name = "状态", readConverterExp = "0=正常,1=停用")
	private String status;

	public Long getDictId() {
		return dictId;
	}

	public void setDictId(Long dictId) {
		this.dictId = dictId;
	}

	@NotBlank(message = "字典名称不能为空")
	@Size(min = 0, max = 100, message = "字典类型名称长度不能超过100个字符")
	public String getDictName() {
		return dictName;
	}

	public void setDictName(String dictName) {
		this.dictName = dictName;
	}

	@NotBlank(message = "字典类型不能为空")
	@Size(min = 0, max = 100, message = "字典类型类型长度不能超过100个字符")
	public String getDictType() {
		return dictType;
	}

	public void setDictType(String dictType) {
		this.dictType = dictType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
