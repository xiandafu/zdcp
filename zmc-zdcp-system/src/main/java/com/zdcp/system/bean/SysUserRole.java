package com.zdcp.system.bean;

import com.zdcp.core.bean.BaseEntity;

/**
 * 用户和角色关联
 * 
 * @author yweijian
 * @date 2019年8月15日
 * @version 1.0
 */
public class SysUserRole extends BaseEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** 用户ID */
	private Long userId;

	/** 角色ID */
	private Long roleId;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
}
