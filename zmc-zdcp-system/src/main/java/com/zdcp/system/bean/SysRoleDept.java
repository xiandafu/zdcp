package com.zdcp.system.bean;

/**
 * 角色和部门关联
 * 
 * @author yweijian
 * @date 2019年8月16日
 * @version 1.0
 */
public class SysRoleDept {
	/** 角色ID */
	private Long roleId;

	/** 部门ID */
	private Long deptId;

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Long getDeptId() {
		return deptId;
	}

	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}

}
