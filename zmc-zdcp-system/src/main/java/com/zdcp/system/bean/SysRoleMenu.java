package com.zdcp.system.bean;

/**
 * 角色和菜单关联
 * 
 * @author yweijian
 * @date 2019年8月16日
 * @version 1.0
 */
public class SysRoleMenu {
	/** 角色ID */
	private Long roleId;

	/** 菜单ID */
	private Long menuId;

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Long getMenuId() {
		return menuId;
	}

	public void setMenuId(Long menuId) {
		this.menuId = menuId;
	}
}
