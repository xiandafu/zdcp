package com.zdcp.system.bean;

import com.zdcp.core.bean.BaseEntity;

/**
 * 用户和岗位关联
 * 
 * @author yweijian
 * @date 2019年8月15日
 * @version 1.0
 */
public class SysUserPost extends BaseEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** 用户ID */
	private Long userId;

	/** 岗位ID */
	private Long postId;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getPostId() {
		return postId;
	}

	public void setPostId(Long postId) {
		this.postId = postId;
	}

}
