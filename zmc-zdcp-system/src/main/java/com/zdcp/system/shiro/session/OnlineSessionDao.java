package com.zdcp.system.shiro.session;

import java.io.Serializable;
import java.util.Date;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.eis.EnterpriseCacheSessionDAO;
import org.springframework.beans.factory.annotation.Autowired;

import com.zdcp.core.enums.OnlineStatus;
import com.zdcp.system.shiro.service.SysShiroService;
import com.zdcp.system.task.AsyncTaskFactory;


/**
 * 创建、获取、更新、删除、记录用户会话
 * @author yweijian
 * @date 2019年8月20日
 * @version 1.0
 */
public class OnlineSessionDao extends EnterpriseCacheSessionDAO{

	/**
	 * 上次同步数据库的时间戳
	 */
	private static final String ONLINE_SESSION_LAST_RECORD_TIMESTAMP = "ONLINE_SESSION_LAST_RECORD_TIMESTAMP";

	@Autowired
	private SysShiroService sysShiroService;
	
	@Override
    protected Session doReadSession(Serializable sessionId){
        return sysShiroService.getSession(sessionId);
    }

	/**
	 * 删除会话；当会话过期/停止（如用户退出时）属性等会调用
	 */
	@Override
	protected void doDelete(Session session) {
		OnlineSession onlineSession = (OnlineSession) session;
		if (null == onlineSession) {
			return;
		}
		onlineSession.setStatus(OnlineStatus.OFF_LINE);
		sysShiroService.deleteSession(onlineSession);
	}
	
	/**
	 * 记录会话；如更新会话最后访问时间/停止会话/设置超时时间/设置移除属性等会调用
	 */
	public void recordOnlineSession(OnlineSession onlineSession) {
		Date lastRecordTimestamp = (Date) onlineSession.getAttribute(ONLINE_SESSION_LAST_RECORD_TIMESTAMP);
		if (lastRecordTimestamp != null) {
			boolean needSync = true;
			long deltaTime = onlineSession.getLastAccessTime().getTime() - lastRecordTimestamp.getTime();
			//同步session到数据库的周期1分钟
			if (deltaTime < 1 * 60 * 1000) {
				// 时间差不足 无需同步
				needSync = false;
			}
			// isGuest = true 访客
			boolean isGuest = onlineSession.getUserId() == null || onlineSession.getUserId() == 0L;

			// session 数据变更了 同步
			if (isGuest == false && onlineSession.isAttributeChanged()) {
				needSync = true;
			}

			if (needSync == false) {
				return;
			}
		}
		// 更新上次同步数据库时间
		onlineSession.setAttribute(ONLINE_SESSION_LAST_RECORD_TIMESTAMP, onlineSession.getLastAccessTime());
		// 更新完后 重置标识
		if (onlineSession.isAttributeChanged()) {
			onlineSession.resetAttributeChanged();
		}
		AsyncTaskFactory.recordOnlineSession(onlineSession);
	}
}
