package com.zdcp.system.shiro.service;

import java.io.Serializable;
import org.apache.shiro.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zdcp.system.bean.SysUserOnline;
import com.zdcp.system.service.ISysUserOnlineService;
import com.zdcp.system.shiro.session.OnlineSession;
import com.zdcp.util.StringUtils;

/**
 * 同步用户会话信息服务
 * 
 * @author yweijian
 * @date 2019年8月20日
 * @version 1.0
 */
@Component
public class SysShiroService {
	
	@Autowired
	private ISysUserOnlineService onlineService;

	/**
	 * 删除会话
	 *
	 * @param onlineSession 会话信息
	 *            
	 */
	public void deleteSession(OnlineSession onlineSession) {
		onlineService.deleteOnlineById(String.valueOf(onlineSession.getId()));
	}

	/**
	 * 获取会话信息
	 *
	 * @param sessionId
	 * @return
	 */
	public Session getSession(Serializable sessionId) {
		SysUserOnline userOnline = onlineService.selectOnlineById(String.valueOf(sessionId));
		return StringUtils.isNull(userOnline) ? null : createSession(userOnline);
	}
	
	/**
	 * 创建会话信息
	 * @param userOnline
	 * @return
	 */
	public Session createSession(SysUserOnline userOnline) {
		OnlineSession onlineSession = new OnlineSession();
		if (StringUtils.isNotNull(userOnline)) {
			onlineSession.setId(userOnline.getSessionId());
			onlineSession.setHost(userOnline.getIpaddr());
			onlineSession.setBrowser(userOnline.getBrowser());
			onlineSession.setOs(userOnline.getOs());
			onlineSession.setDeptName(userOnline.getDeptName());
			onlineSession.setLoginName(userOnline.getLoginName());
			onlineSession.setStartTimestamp(userOnline.getCreateTime());
			onlineSession.setLastAccessTime(userOnline.getLastAccessTime());
			onlineSession.setTimeout(userOnline.getExpireTime());
		}
		return onlineSession;
	}
}
