package com.zdcp.system.shiro.service;

import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.PostConstruct;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.zdcp.core.constant.Constants;
import com.zdcp.core.constant.ShiroConstants;
import com.zdcp.core.exception.BaseException;
import com.zdcp.core.enums.DataStatus;
import com.zdcp.system.bean.SysUser;
import com.zdcp.system.constant.SysConstants;
import com.zdcp.system.service.ISysUserService;
import com.zdcp.system.shiro.util.ShiroUtils;
import com.zdcp.system.task.AsyncTaskFactory;
import com.zdcp.util.MessageUtils;
import com.zdcp.util.ServletUtils;

/**
 * 用户登录、校验处理服务
 * 
 * @author yweijian
 * @date 2019年8月18日
 * @version 1.0
 */
@Component
public class SysLoginService {

	@Autowired
	private ISysUserService userService;
	
	@Autowired
	private CacheManager cacheManager;

	private Cache<String, AtomicInteger> loginRecordCache;

	/**
	 * 密码输入错误最多重复次数
	 */
	private Integer maxRetryCount = 5;

	@PostConstruct
	public void init() {
		loginRecordCache = cacheManager.getCache(ShiroConstants.LOGIN_RECORD_CACHE);
	}

	/**
	 * 登录
	 */
	public SysUser login(String username, String password) {
		// 验证码校验
		if (!StringUtils.isEmpty(ServletUtils.getRequest().getAttribute(ShiroConstants.CURRENT_CAPTCHA))) {
			AsyncTaskFactory.recordLoginInfo(username, Constants.LOGIN_FAIL,MessageUtils.message("user.kaptcha.error"));
			throw new BaseException(MessageUtils.message("user.kaptcha.error"));
		}
		// 用户名或密码为空 错误
		if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
			AsyncTaskFactory.recordLoginInfo(username, Constants.LOGIN_FAIL, MessageUtils.message("not.null"));
			throw new BaseException(MessageUtils.message("not.null"));
		}
		// 密码如果不在指定范围内 错误
		if (password.length() < SysConstants.PASSWORD_MIN_LENGTH || password.length() > SysConstants.PASSWORD_MAX_LENGTH) {
			AsyncTaskFactory.recordLoginInfo(username, Constants.LOGIN_FAIL, MessageUtils.message("user.password.not.match"));
			throw new BaseException(MessageUtils.message("user.password.not.match"));
		}

		// 用户名不在指定范围内 错误
		if (username.length() < SysConstants.USERNAME_MIN_LENGTH || username.length() > SysConstants.USERNAME_MAX_LENGTH) {
			AsyncTaskFactory.recordLoginInfo(username, Constants.LOGIN_FAIL, MessageUtils.message("user.password.not.match"));
			throw new BaseException(MessageUtils.message("user.password.not.match"));
		}

		// 查询用户信息
		SysUser user = userService.selectUserByLoginName(username);

		if (user == null && maybeMobilePhoneNumber(username)) {
			user = userService.selectUserByPhoneNumber(username);
		}

		if (user == null && maybeEmail(username)) {
			user = userService.selectUserByEmail(username);
		}

		if (user == null) {
			AsyncTaskFactory.recordLoginInfo(username, Constants.LOGIN_FAIL, MessageUtils.message("user.not.exists"));
			throw new BaseException(MessageUtils.message("user.not.exists"));
		}

		if (DataStatus.DELETED.getCode().equals(user.getDelFlag())) {
			AsyncTaskFactory.recordLoginInfo(username, Constants.LOGIN_FAIL, MessageUtils.message("user.password.delete"));
			throw new BaseException(MessageUtils.message("user.password.delete"));
		}

		if (DataStatus.DISABLE.getCode().equals(user.getStatus())) {
			AsyncTaskFactory.recordLoginInfo(username, Constants.LOGIN_FAIL, MessageUtils.message("user.blocked", user.getRemark()));
			throw new BaseException(MessageUtils.message("user.blocked", user.getRemark()));
		}

		this.validate(user, password);
		user.setLoginIp(ShiroUtils.getIp());
		user.setLoginDate(new Date());
		userService.updateUserInfo(user);
		
		AsyncTaskFactory.recordLoginInfo(username, Constants.LOGIN_SUCCESS, MessageUtils.message("user.login.success"));
		
		return userService.selectUserById(user.getUserId());
	}
	
	/**
	 * 验证
	 * @param user
	 * @param password
	 */
	public void validate(SysUser user, String password) {
		String loginName = user.getLoginName();

		AtomicInteger retryCount = loginRecordCache.get(loginName);

		if (retryCount == null) {
			retryCount = new AtomicInteger(0);
			loginRecordCache.put(loginName, retryCount);
		}
		if (retryCount.incrementAndGet() > maxRetryCount) {
			AsyncTaskFactory.recordLoginInfo(loginName, Constants.LOGIN_FAIL, MessageUtils.message("user.password.retry.limit.exceed", maxRetryCount));
			throw new BaseException(MessageUtils.message("user.password.retry.limit.exceed", maxRetryCount));
		}

		if (!matches(user, password)) {
			AsyncTaskFactory.recordLoginInfo(loginName, Constants.LOGIN_FAIL, MessageUtils.message("user.password.retry.limit.count", retryCount));
			loginRecordCache.put(loginName, retryCount);
			throw new BaseException(MessageUtils.message("user.password.not.match"));
		} else {
			clearLoginRecordCache(loginName);
		}
	}

	/**
	 * 密码验证
	 * @param user
	 * @param newPassword
	 * @return
	 */
	public boolean matches(SysUser user, String newPassword) {
		return user.getPassword().equals(encryptPassword(user.getLoginName(), newPassword, user.getSalt()));
	}

	/**
	 * 清除缓存记录
	 * @param username
	 */
	public void clearLoginRecordCache(String username) {
		loginRecordCache.remove(username);
	}

	/**
	 * 密码加密
	 * @param username
	 * @param password
	 * @param salt
	 * @return
	 */
	public String encryptPassword(String username, String password, String salt) {
		return new Md5Hash(username + password + salt).toHex().toString();
	}
	
	private boolean maybeEmail(String username) {
		if (!username.matches(SysConstants.EMAIL_PATTERN)) {
			return false;
		}
		return true;
	}

	private boolean maybeMobilePhoneNumber(String username) {
		if (!username.matches(SysConstants.MOBILE_PHONE_NUMBER_PATTERN)) {
			return false;
		}
		return true;
	}

}
