package com.zdcp.system.mapper;

import java.util.List;

import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import com.zdcp.system.bean.SysDictType;

/**
 * 字典表 数据层
 * 
 * @author yweijian
 * @date 2019年8月16日
 * @version 1.0
 */
@SqlResource("system.sysDictType")
public interface SysDictTypeMapperDao extends BaseMapper<SysDictType> {
	/**
	 * 根据条件分页查询字典类型
	 * 
	 * @param dictType
	 *            字典类型信息
	 * @return 字典类型集合信息
	 */
	public List<SysDictType> selectDictTypeList(PageQuery<SysDictType> query);
	
	/**
	 * 根据条件查询字典类型
	 * 
	 * @param dictType
	 *            字典类型信息
	 * @return 字典类型集合信息
	 */
	public List<SysDictType> selectDictTypeList(SysDictType dictType);

	/**
	 * 根据所有字典类型
	 * 
	 * @return 字典类型集合信息
	 */
	public List<SysDictType> selectDictTypeAll();

	/**
	 * 根据字典类型ID查询信息
	 * 
	 * @param dictId
	 *            字典类型ID
	 * @return 字典类型
	 */
	public SysDictType selectDictTypeById(Long dictId);

	/**
	 * 根据字典类型查询信息
	 * 
	 * @param dictType
	 *            字典类型
	 * @return 字典类型
	 */
	public SysDictType selectDictTypeByType(String dictType);

	/**
	 * 通过字典ID删除字典信息
	 * 
	 * @param dictId
	 *            字典ID
	 * @return 结果
	 */
	public int deleteDictTypeById(Long dictId);

	/**
	 * 批量删除字典类型
	 * 
	 * @param ids
	 *            需要删除的数据
	 * @return 结果
	 */
	public int deleteDictTypeByIds(String[] ids);

	/**
	 * 校验字典类型称是否唯一
	 * 
	 * @param dictType
	 *            字典类型
	 * @return 结果
	 */
	public SysDictType checkDictTypeUnique(String dictType);
}
