package com.zdcp.system.mapper;

import java.util.List;

import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import com.zdcp.system.bean.SysOperLog;

/**
 * 操作日志
 * 
 * @author yweijian
 * @date 2019年8月16日
 * @version 1.0
 */
@SqlResource("system.sysOperLog")
public interface SysOperLogMapperDao extends BaseMapper<SysOperLog> {

	/**
	 * 分页查询系统操作日志集合
	 * 
	 * @param operLog
	 *            操作日志对象
	 * @return 操作日志集合
	 */
	public List<SysOperLog> selectOperLogList(PageQuery<SysOperLog> query);
	
	/**
	 * 查询系统操作日志集合
	 * 
	 * @param operLog
	 *            操作日志对象
	 * @return 操作日志集合
	 */
	public List<SysOperLog> selectOperLogList(SysOperLog operLog);

	/**
	 * 批量删除系统操作日志
	 * 
	 * @param ids
	 *            需要删除的数据
	 * @return 结果
	 */
	public int deleteOperLogByIds(String[] ids);

	/**
	 * 查询操作日志详细
	 * 
	 * @param operId
	 *            操作ID
	 * @return 操作日志对象
	 */
	public SysOperLog selectOperLogById(Long operId);

	/**
	 * 清空操作日志
	 */
	public void cleanOperLog();
}
