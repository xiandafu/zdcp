package com.zdcp.system.mapper;

import java.util.List;

import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import com.zdcp.system.bean.SysLoginInfo;

/**
 * 系统访问日志情况信息
 * 
 * @author yweijian
 * @date 2019年8月16日
 * @version 1.0
 */
@SqlResource("system.sysLoginInfo")
public interface SysLoginInfoMapperDao extends BaseMapper<SysLoginInfo> {

	/**
	 * 分页查询系统登录日志集合
	 * 
	 * @param loginInfo
	 *            访问日志对象
	 * @return 登录记录集合
	 */
	public List<SysLoginInfo> selectLoginInfoList(PageQuery<SysLoginInfo> query);
	
	/**
	 * 查询系统登录日志集合
	 * 
	 * @param loginInfo
	 *            访问日志对象
	 * @return 登录记录集合
	 */
	public List<SysLoginInfo> selectLoginInfoList(SysLoginInfo logininfo);

	/**
	 * 批量删除系统登录日志
	 * 
	 * @param ids
	 *            需要删除的数据
	 * @return 结果
	 */
	public int deleteLoginInfoByIds(String[] ids);

	/**
	 * 清空系统登录日志
	 * 
	 * @return 结果
	 */
	public int cleanLoginInfo();
}
