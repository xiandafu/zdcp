package com.zdcp.system.mapper;

import java.util.List;

import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import com.zdcp.system.bean.SysPost;

/**
 * 岗位信息 映射
 * 
 * @author yweijian
 * @date 2019年8月15日
 * @version 1.0
 */
@SqlResource("system.sysPost")
public interface SysPostMapperDao extends BaseMapper<SysPost> {
	/**
	 * 查询岗位数据集合
	 * 
	 * @param post
	 *            岗位信息
	 * @return 岗位数据集合
	 */
	public List<SysPost> selectPostList(SysPost post);
	
	/**
	 * 查询岗位数据集合
	 * 
	 * @param post
	 *            岗位信息
	 * @return 岗位数据集合
	 */
	public List<SysPost> selectPostList(PageQuery<SysPost> query);

	/**
	 * 查询所有岗位
	 * 
	 * @return 岗位列表
	 */
	public List<SysPost> selectPostAll();

	/**
	 * 根据用户ID查询岗位
	 * 
	 * @param userId
	 *            用户ID
	 * @return 岗位列表
	 */
	public List<SysPost> selectPostsByUserId(Long userId);

	/**
	 * 通过岗位ID查询岗位信息
	 * 
	 * @param postId
	 *            岗位ID
	 * @return 角色对象信息
	 */
	public SysPost selectPostById(Long postId);

	/**
	 * 批量删除岗位信息
	 * 
	 * @param ids
	 *            需要删除的数据ID
	 * @return 结果
	 */
	public int deletePostByIds(String[] ids);

	/**
	 * 校验岗位名称
	 * 
	 * @param postName
	 *            岗位名称
	 * @return 结果
	 */
	public SysPost checkPostNameUnique(String postName);

	/**
	 * 校验岗位编码
	 * 
	 * @param postCode
	 *            岗位编码
	 * @return 结果
	 */
	public SysPost checkPostCodeUnique(String postCode);
}
