package com.zdcp.system.mapper;

import java.util.List;

import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import com.zdcp.system.bean.SysUserOnline;

/**
 * 在线用户
 * 
 * @author yweijian
 * @date 2019年8月16日
 * @version 1.0
 */
@SqlResource("system.sysUserOnline")
public interface SysUserOnlineMapperDao extends BaseMapper<SysUserOnline> {
	/**
	 * 通过会话序号查询信息
	 * 
	 * @param sessionId
	 *            会话ID
	 * @return 在线用户信息
	 */
	public SysUserOnline selectOnlineById(String sessionId);

	/**
	 * 通过会话序号删除信息
	 * 
	 * @param sessionId
	 *            会话ID
	 * @return 在线用户信息
	 */
	public int deleteOnlineById(String sessionId);

	/**
	 * 查询会话集合
	 * 
	 * @param userOnline
	 *            会话参数
	 * @return 会话集合
	 */
	public List<SysUserOnline> selectUserOnlineList(PageQuery<SysUserOnline> query);

	/**
	 * 查询过期会话集合
	 * 
	 * @param lastAccessTime
	 *            过期时间
	 * @return 会话集合
	 */
	public List<SysUserOnline> selectOnlineByExpired(String lastAccessTime);
	
	/**
	 * 通过会话序号更新信息
	 * 
	 * @param sessionId
	 *            会话ID
	 * @return 在线用户信息
	 */
	public int updateOnline(SysUserOnline online);
}
