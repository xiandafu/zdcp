package com.zdcp.system.mapper;

import java.util.List;

import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.mapper.BaseMapper;

import com.zdcp.system.bean.SysUserPost;

/**
 * 用户与岗位关联映射
 * 
 * @author yweijian
 * @date 2019年8月15日
 * @version 1.0
 */
@SqlResource("system.sysUserPost")
public interface SysUserPostMapperDao extends BaseMapper<SysUserPost> {
	/**
	 * 通过用户ID删除用户和岗位关联
	 * 
	 * @param userId
	 *            用户ID
	 * @return 结果
	 */
	public int deleteUserPostByUserId(Long userId);

	/**
	 * 通过岗位ID查询岗位使用数量
	 * 
	 * @param postId
	 *            岗位ID
	 * @return 结果
	 */
	public int countUserPostById(Long postId);

	/**
	 * 批量删除用户和岗位关联
	 * 
	 * @param ids
	 *            需要删除的数据ID
	 * @return 结果
	 */
	public int deleteUserPost(Long[] ids);

	/**
	 * 批量新增用户岗位信息
	 * 
	 * @param userPostList
	 *            用户角色列表
	 * @return 结果
	 */
	public int batchUserPost(List<SysUserPost> userPostList);
}
