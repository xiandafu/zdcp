package com.zdcp.system.mapper;

import java.util.List;

import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import com.zdcp.system.bean.SysConfig;

/**
 * 参数配置 映射
 * 
 * @author yweijian
 * @date 2019年8月15日
 * @version 1.0
 */
@SqlResource("system.sysConfig")
public interface SysConfigMapperDao extends BaseMapper<SysConfig> {
	/**
	 * 查询参数配置信息
	 * 
	 * @param config
	 *            参数配置信息
	 * @return 参数配置信息
	 */
	public SysConfig selectConfig(SysConfig config);

	/**
	 * 查询参数配置列表
	 * 
	 * @param config
	 *            参数配置信息
	 * @return 参数配置集合
	 */
	public List<SysConfig> selectConfigList(SysConfig config);

	/**
	 * 分页查询参数配置列表
	 * 
	 * @param config
	 *            参数配置信息
	 * @return 参数配置集合
	 */
	public List<SysConfig> selectConfigList(PageQuery<SysConfig> query);
	
	/**
	 * 根据键名查询参数配置信息
	 * 
	 * @param configKey
	 *            参数键名
	 * @return 参数配置信息
	 */
	public SysConfig checkConfigKeyUnique(String configKey);

	/**
	 * 批量删除参数配置
	 * 
	 * @param ids
	 *            需要删除的数据ID
	 * @return 结果
	 */
	public int deleteConfigByIds(String[] ids);
}