package com.zdcp.system.mapper;

import java.util.List;

import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import com.zdcp.system.bean.SysNotice;

/**
 * 公告
 * 
 * @author yweijian
 * @date 2019年8月16日
 * @version 1.0
 */
@SqlResource("system.sysNotice")
public interface SysNoticeMapperDao extends BaseMapper<SysNotice> {
	/**
	 * 查询公告信息
	 * 
	 * @param noticeId
	 *            公告ID
	 * @return 公告信息
	 */
	public SysNotice selectNoticeById(Long noticeId);

	/**
	 * 查询公告列表
	 * 
	 * @param notice
	 *            公告信息
	 * @return 公告集合
	 */
	public List<SysNotice> selectNoticeList(PageQuery<SysNotice> query);

	/**
	 * 批量删除公告
	 * 
	 * @param noticeIds
	 *            需要删除的数据ID
	 * @return 结果
	 */
	public int deleteNoticeByIds(String[] ids);
}