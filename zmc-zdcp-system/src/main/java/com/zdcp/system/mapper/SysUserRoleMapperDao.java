package com.zdcp.system.mapper;

import java.util.List;

import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.mapper.BaseMapper;

import com.zdcp.system.bean.SysUserRole;

/**
 * 用户与角色管理映射
 * 
 * @author yweijian
 * @date 2019年8月15日
 * @version 1.0
 */
@SqlResource("system.sysUserRole")
public interface SysUserRoleMapperDao extends BaseMapper<SysUserRole> {
	/**
	 * 通过用户ID删除用户和角色关联
	 * 
	 * @param userId
	 *            用户ID
	 * @return 结果
	 */
	public int deleteUserRoleByUserId(Long userId);

	/**
	 * 批量删除用户和角色关联
	 * 
	 * @param ids
	 *            需要删除的数据ID
	 * @return 结果
	 */
	public int deleteUserRole(Long[] ids);

	/**
	 * 通过角色ID查询角色使用数量
	 * 
	 * @param roleId
	 *            角色ID
	 * @return 结果
	 */
	public int countUserRoleByRoleId(Long roleId);

	/**
	 * 批量新增用户角色信息
	 * 
	 * @param userRoleList
	 *            用户角色列表
	 * @return 结果
	 */
	public int batchUserRole(List<SysUserRole> userRoleList);

	/**
	 * 删除用户和角色关联信息
	 * 
	 * @param userRole
	 *            用户和角色关联信息
	 * @return 结果
	 */
	public int deleteUserRoleInfo(SysUserRole userRole);

	/**
	 * 批量取消授权用户角色
	 * 
	 * @param roleId
	 *            角色ID
	 * @param userIds
	 *            需要删除的用户数据ID
	 * @return 结果
	 */
	public int deleteUserRoleInfos(@Param("roleId") Long roleId, @Param("userIds") Long[] userIds);
}
