package com.zdcp.system.mapper;

import java.util.List;

import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import com.zdcp.system.bean.SysRole;

/**
 * 角色表映射
 * 
 * @author yweijian
 * @date 2019年8月15日
 * @version 1.0
 */
@SqlResource("system.sysRole")
public interface SysRoleMapperDao extends BaseMapper<SysRole> {
	/**
	 * 根据条件查询角色数据
	 * 
	 * @param role
	 *            角色信息
	 * @return 角色数据集合信息
	 */
	public List<SysRole> selectRoleList(SysRole role);
	
	/**
	 * 根据条件分页查询角色数据
	 * @param query
	 * @return
	 */
	public List<SysRole> selectRoleList(PageQuery<SysRole> query);

	/**
	 * 根据用户ID查询角色
	 * 
	 * @param userId
	 *            用户ID
	 * @return 角色列表
	 */
	public List<SysRole> selectRolesByUserId(Long userId);

	/**
	 * 通过角色ID查询角色
	 * 
	 * @param roleId
	 *            角色ID
	 * @return 角色对象信息
	 */
	public SysRole selectRoleById(Long roleId);

	/**
	 * 通过角色ID删除角色
	 * 
	 * @param roleId
	 *            角色ID
	 * @return 结果
	 */
	public int deleteRoleById(Long roleId);

	/**
	 * 批量角色用户信息
	 * 
	 * @param ids
	 *            需要删除的数据ID
	 * @return 结果
	 */
	public int deleteRoleByIds(String[] ids);

	/**
	 * 校验角色名称是否唯一
	 * 
	 * @param roleName
	 *            角色名称
	 * @return 角色信息
	 */
	public SysRole checkRoleNameUnique(String roleName);

	/**
	 * 校验角色权限是否唯一
	 * 
	 * @param roleKey
	 *            角色权限
	 * @return 角色信息
	 */
	public SysRole checkRoleKeyUnique(String roleKey);
}
