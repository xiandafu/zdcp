package com.zdcp.system.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.beetl.sql.core.engine.PageQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zdcp.core.annotation.DataScope;
import com.zdcp.core.exception.BusinessException;
import com.zdcp.system.constant.SysConstants;
import com.zdcp.system.bean.SysDept;
import com.zdcp.system.bean.SysPost;
import com.zdcp.system.bean.SysRole;
import com.zdcp.system.bean.SysUser;
import com.zdcp.system.bean.SysUserPost;
import com.zdcp.system.bean.SysUserRole;
import com.zdcp.system.mapper.SysDeptMapperDao;
import com.zdcp.system.mapper.SysPostMapperDao;
import com.zdcp.system.mapper.SysRoleMapperDao;
import com.zdcp.system.mapper.SysUserMapperDao;
import com.zdcp.system.mapper.SysUserPostMapperDao;
import com.zdcp.system.mapper.SysUserRoleMapperDao;
import com.zdcp.system.service.ISysConfigService;
import com.zdcp.system.service.ISysUserService;
import com.zdcp.util.Md5Utils;
import com.zdcp.util.StringUtils;

/**
 * 用户实现
 * @author yweijian
 * @date 2019年8月23日
 * @version 1.0
 */
@Service
public class SysUserServiceImpl implements ISysUserService {
	
	private static final Logger log = LoggerFactory.getLogger(SysUserServiceImpl.class);

	@Autowired
	private SysUserMapperDao userMapper;

	@Autowired
	private SysRoleMapperDao roleMapper;

	@Autowired
	private SysPostMapperDao postMapper;
	
	@Autowired
	private SysDeptMapperDao deptMapper;

	@Autowired
	private SysUserPostMapperDao userPostMapper;

	@Autowired
	private SysUserRoleMapperDao userRoleMapper;

	@Autowired
	private ISysConfigService configService;

	
	/**
	 * 根据条件分页查询用户列表
	 * 
	 * @param user
	 *            用户信息
	 * @return 用户信息集合信息
	 */
	@Override
	@DataScope(deptAlias = "d", userAlias = "u")
	public List<SysUser> selectUserList(SysUser user) {
		return userMapper.selectUserList(user);
	}
	
	/**
	 * 根据条件分页查询用户列表
	 * 
	 * @param query
	 *            用户信息
	 * @return 用户信息集合信息
	 */
	@Override
	@DataScope(deptAlias = "d", userAlias = "u")
	public List<SysUser> selectUserList(PageQuery<SysUser> query) {
		userMapper.selectUserList(query);
		return query.getList();
	}

	/**
	 * 根据条件分页查询已分配用户角色列表
	 * 
	 * @param user
	 *            用户信息
	 * @return 用户信息集合信息
	 */
	@DataScope(deptAlias = "d", userAlias = "u")
	public List<SysUser> selectAllocatedList(PageQuery<SysUser> query) {
		userMapper.selectAllocatedList(query);
		return query.getList();
	}

	/**
	 * 根据条件分页查询未分配用户角色列表
	 * 
	 * @param user
	 *            用户信息
	 * @return 用户信息集合信息
	 */
	@DataScope(deptAlias = "d", userAlias = "u")
	public List<SysUser> selectUnallocatedList(PageQuery<SysUser> query) {
		userMapper.selectUnallocatedList(query);
		return query.getList();
	}

	/**
	 * 通过用户名查询用户
	 * 
	 * @param userName
	 *            用户名
	 * @return 用户对象信息
	 */
	@Override
	public SysUser selectUserByLoginName(String userName) {
		return userMapper.selectUserByLoginName(userName);
	}

	/**
	 * 通过手机号码查询用户
	 * 
	 * @param phoneNumber
	 *            手机号码
	 * @return 用户对象信息
	 */
	@Override
	public SysUser selectUserByPhoneNumber(String phoneNumber) {
		return userMapper.selectUserByPhoneNumber(phoneNumber);
	}

	/**
	 * 通过邮箱查询用户
	 * 
	 * @param email
	 *            邮箱
	 * @return 用户对象信息
	 */
	@Override
	public SysUser selectUserByEmail(String email) {
		return userMapper.selectUserByEmail(email);
	}

	/**
	 * 通过用户ID查询用户
	 * 
	 * @param userId
	 *            用户ID
	 * @return 用户对象信息
	 */
	@Override
	public SysUser selectUserById(Long userId) {
		SysUser user = userMapper.selectUserById(userId);
		SysDept dept = deptMapper.selectDeptById(user.getDeptId());
		List<SysRole> roles = roleMapper.selectRolesByUserId(userId);
		user.setDept(dept);
		user.setRoles(roles);
		return user;
	}

	/**
	 * 通过用户ID删除用户
	 * 
	 * @param userId
	 *            用户ID
	 * @return 结果
	 */
	@Override
	@Transactional
	public int deleteUserById(Long userId) {
		// 删除用户与角色关联
		userRoleMapper.deleteUserRoleByUserId(userId);
		// 删除用户与岗位表
		userPostMapper.deleteUserPostByUserId(userId);
		//删除用户
		userMapper.deleteUserById(userId);
		return 1;
	}

	/**
	 * 批量删除用户信息
	 * 
	 * @param ids
	 *            需要删除的数据ID
	 * @return 结果
	 */
	@Override
	@Transactional
	public int deleteUserByIds(String[] ids) throws BusinessException {
		for (String userId : ids) {
			if (SysUser.isAdmin(Long.parseLong(userId))) {
				throw new BusinessException("不允许删除超级管理员用户");
			}
		}
		return userMapper.deleteUserByIds(ids);
	}

	/**
	 * 新增保存用户信息
	 * 
	 * @param user
	 *            用户信息
	 * @return 结果
	 */
	@Override
	@Transactional
	public int insertUser(SysUser user) {
		user.setCreateTime(new Date());
		// 新增用户信息
		userMapper.insert(user, true);
		// 新增用户岗位关联
		insertUserPost(user);
		// 新增用户与角色管理
		insertUserRole(user);
		return 1;
	}

	/**
	 * 修改保存用户信息
	 * 
	 * @param user
	 *            用户信息
	 * @return 结果
	 */
	@Override
	@Transactional
	public int updateUser(SysUser user) {
		Long userId = user.getUserId();
		// 删除用户与角色关联
		userRoleMapper.deleteUserRoleByUserId(userId);
		// 新增用户与角色管理
		insertUserRole(user);
		// 删除用户与岗位关联
		userPostMapper.deleteUserPostByUserId(userId);
		// 新增用户与岗位管理
		insertUserPost(user);
		user.setUpdateTime(new Date());
		userMapper.updateTemplateById(user);
		return 1;
	}

	/**
	 * 修改用户个人详细信息
	 * 
	 * @param user
	 *            用户信息
	 * @return 结果
	 */
	@Override
	@Transactional
	public int updateUserInfo(SysUser user) {
		return userMapper.updateTemplateById(user);
	}

	/**
	 * 修改用户密码
	 * 
	 * @param user
	 *            用户信息
	 * @return 结果
	 */
	@Override
	@Transactional
	public int resetUserPwd(SysUser user) {
		return userMapper.updateTemplateById(user);
	}

	/**
	 * 新增用户角色信息
	 * 
	 * @param user
	 *            用户对象
	 */
	@Transactional
	public void insertUserRole(SysUser user) {
		Long[] roles = user.getRoleIds();
		if (StringUtils.isNotNull(roles)) {
			// 新增用户与角色管理
			List<SysUserRole> list = new ArrayList<SysUserRole>();
			for (Long roleId : roles) {
				SysUserRole ur = new SysUserRole();
				ur.setUserId(user.getUserId());
				ur.setRoleId(roleId);
				list.add(ur);
			}
			if (list.size() > 0) {
				userRoleMapper.batchUserRole(list);
			}
		}
	}

	/**
	 * 新增用户岗位信息
	 * 
	 * @param user
	 *            用户对象
	 */
	@Transactional
	public void insertUserPost(SysUser user) {
		Long[] posts = user.getPostIds();
		if (StringUtils.isNotNull(posts)) {
			// 新增用户与岗位管理
			List<SysUserPost> list = new ArrayList<SysUserPost>();
			for (Long postId : posts) {
				SysUserPost up = new SysUserPost();
				up.setUserId(user.getUserId());
				up.setPostId(postId);
				list.add(up);
			}
			if (list.size() > 0) {
				userPostMapper.batchUserPost(list);
			}
		}
	}

	/**
	 * 校验登录名称是否唯一
	 * 
	 * @param loginName
	 *            用户名
	 * @return
	 */
	@Override
	public String checkLoginNameUnique(String loginName) {
		int count = userMapper.checkLoginNameUnique(loginName);
		if (count > 0) {
			return SysConstants.NOT_UNIQUE;
		}
		return SysConstants.IS_UNIQUE;
	}

	/**
	 * 校验用户名称是否唯一
	 *
	 * @param user
	 *            用户信息
	 * @return
	 */
	@Override
	public String checkPhoneUnique(SysUser user) {
		Long userId = StringUtils.isNull(user.getUserId()) ? -1L : user.getUserId();
		SysUser info = userMapper.checkPhoneUnique(user.getPhonenumber());
		if (StringUtils.isNotNull(info) && info.getUserId().longValue() != userId.longValue()) {
			return SysConstants.NOT_UNIQUE;
		}
		return SysConstants.IS_UNIQUE;
	}

	/**
	 * 校验email是否唯一
	 *
	 * @param user
	 *            用户信息
	 * @return
	 */
	@Override
	public String checkEmailUnique(SysUser user) {
		Long userId = StringUtils.isNull(user.getUserId()) ? -1L : user.getUserId();
		SysUser info = userMapper.checkEmailUnique(user.getEmail());
		if (StringUtils.isNotNull(info) && info.getUserId().longValue() != userId.longValue()) {
			return SysConstants.NOT_UNIQUE;
		}
		return SysConstants.IS_UNIQUE;
	}

	/**
	 * 查询用户所属角色组
	 * 
	 * @param userId
	 *            用户ID
	 * @return 结果
	 */
	@Override
	public String selectUserRoleGroup(Long userId) {
		List<SysRole> list = roleMapper.selectRolesByUserId(userId);
		StringBuffer idsStr = new StringBuffer();
		for (SysRole role : list) {
			idsStr.append(role.getRoleName()).append(",");
		}
		if (StringUtils.isNotEmpty(idsStr.toString())) {
			return idsStr.substring(0, idsStr.length() - 1);
		}
		return idsStr.toString();
	}

	/**
	 * 查询用户所属岗位组
	 * 
	 * @param userId
	 *            用户ID
	 * @return 结果
	 */
	@Override
	public String selectUserPostGroup(Long userId) {
		List<SysPost> list = postMapper.selectPostsByUserId(userId);
		StringBuffer idsStr = new StringBuffer();
		for (SysPost post : list) {
			idsStr.append(post.getPostName()).append(",");
		}
		if (StringUtils.isNotEmpty(idsStr.toString())) {
			return idsStr.substring(0, idsStr.length() - 1);
		}
		return idsStr.toString();
	}

	/**
	 * 导入用户数据
	 * 
	 * @param userList
	 *            用户数据列表
	 * @param isUpdateSupport
	 *            是否更新支持，如果已存在，则进行更新数据
	 * @param operName
	 *            操作用户
	 * @return 结果
	 */
	@Override
	public String importUser(List<SysUser> userList, Boolean isUpdateSupport, String operName) {
		if (StringUtils.isNull(userList) || userList.size() == 0) {
			throw new BusinessException("导入用户数据不能为空！");
		}
		int successNum = 0;
		int failureNum = 0;
		StringBuilder successMsg = new StringBuilder();
		StringBuilder failureMsg = new StringBuilder();
		String password = configService.selectConfigByKey("sys.user.initPassword");
		for (SysUser user : userList) {
			try {
				// 验证是否存在这个用户
				SysUser u = userMapper.selectUserByLoginName(user.getLoginName());
				if (StringUtils.isNull(u)) {
					user.setPassword(Md5Utils.hash(user.getLoginName() + password));
					user.setCreateBy(operName);
					this.insertUser(user);
					successNum++;
					successMsg.append("<br/>" + successNum + "、账号 " + user.getLoginName() + " 导入成功");
				} else if (isUpdateSupport) {
					user.setUpdateBy(operName);
					this.updateUser(user);
					successNum++;
					successMsg.append("<br/>" + successNum + "、账号 " + user.getLoginName() + " 更新成功");
				} else {
					failureNum++;
					failureMsg.append("<br/>" + failureNum + "、账号 " + user.getLoginName() + " 已存在");
				}
			} catch (Exception e) {
				failureNum++;
				String msg = "<br/>" + failureNum + "、账号 " + user.getLoginName() + " 导入失败：";
				failureMsg.append(msg + e.getMessage());
				log.error(msg, e);
			}
		}
		if (failureNum > 0) {
			failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
			throw new BusinessException(failureMsg.toString());
		} else {
			successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
		}
		return successMsg.toString();
	}

	/**
	 * 用户状态修改
	 * 
	 * @param user
	 *            用户信息
	 * @return 结果
	 */
	@Override
	@Transactional
	public int changeStatus(SysUser user) {
		if (SysUser.isAdmin(user.getUserId())) {
			throw new BusinessException("不允许修改超级管理员用户");
		}
		return userMapper.updateTemplateById(user);
	}
}
