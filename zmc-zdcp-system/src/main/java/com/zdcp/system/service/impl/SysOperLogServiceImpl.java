package com.zdcp.system.service.impl;

import java.util.Date;
import java.util.List;

import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zdcp.system.bean.SysOperLog;
import com.zdcp.system.mapper.SysOperLogMapperDao;
import com.zdcp.system.service.ISysOperLogService;

/**
 * 操作日志 服务
 * 
 * @author yweijian
 * @date 2019年8月16日
 * @version 1.0
 */
@Service
public class SysOperLogServiceImpl implements ISysOperLogService {
	@Autowired
	private SysOperLogMapperDao operLogMapper;

	/**
	 * 新增操作日志
	 * 
	 * @param operLog
	 *            操作日志对象
	 */
	@Override
	@Transactional
	public void insertOperlog(SysOperLog operLog) {
		operLog.setOperTime(new Date());
		operLogMapper.insert(operLog, true);
	}

	/**
	 * 分页查询系统操作日志集合
	 * 
	 * @param operLog
	 *            操作日志对象
	 * @return 操作日志集合
	 */
	@Override
	public List<SysOperLog> selectOperLogList(PageQuery<SysOperLog> query) {
		operLogMapper.selectOperLogList(query);
		return query.getList();
	}
	
	/**
	 * 查询系统操作日志集合
	 * 
	 * @param operLog
	 *            操作日志对象
	 * @return 操作日志集合
	 */
	@Override
	public List<SysOperLog> selectOperLogList(SysOperLog operLog) {
		return operLogMapper.selectOperLogList(operLog);
	}

	/**
	 * 批量删除系统操作日志
	 * 
	 * @param ids
	 *            需要删除的数据
	 * @return
	 */
	@Override
	@Transactional
	public int deleteOperLogByIds(String[] ids) {
		return operLogMapper.deleteOperLogByIds(ids);
	}

	/**
	 * 查询操作日志详细
	 * 
	 * @param operId
	 *            操作ID
	 * @return 操作日志对象
	 */
	@Override
	public SysOperLog selectOperLogById(Long operId) {
		return operLogMapper.selectOperLogById(operId);
	}

	/**
	 * 清空操作日志
	 */
	@Override
	public void cleanOperLog() {
		operLogMapper.cleanOperLog();
	}
}
