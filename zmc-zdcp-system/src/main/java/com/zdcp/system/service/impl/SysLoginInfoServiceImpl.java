package com.zdcp.system.service.impl;

import java.util.Date;
import java.util.List;

import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zdcp.system.bean.SysLoginInfo;
import com.zdcp.system.mapper.SysLoginInfoMapperDao;
import com.zdcp.system.service.ISysLoginInfoService;

/**
 * 系统访问日志
 * 
 * @author yweijian
 * @date 2019年8月16日
 * @version 1.0
 */
@Service
public class SysLoginInfoServiceImpl implements ISysLoginInfoService {

	@Autowired
	private SysLoginInfoMapperDao loginInfoMapper;

	/**
	 * 新增系统登录日志
	 * 
	 * @param loginInfo
	 *            访问日志对象
	 */
	@Override
	@Transactional
	public void insertLoginInfo(SysLoginInfo loginInfo) {
		loginInfo.setLoginTime(new Date());
		loginInfoMapper.insert(loginInfo, true);
	}

	/**
	 * 分页查询系统登录日志集合
	 * 
	 * @param loginInfo
	 *            访问日志对象
	 * @return 登录记录集合
	 */
	@Override
	public List<SysLoginInfo> selectLoginInfoList(PageQuery<SysLoginInfo> query) {
		loginInfoMapper.selectLoginInfoList(query);
		return query.getList();
	}
	
	/**
	 * 查询系统登录日志集合
	 * 
	 * @param loginInfo
	 *            访问日志对象
	 * @return 登录记录集合
	 */
	@Override
	public List<SysLoginInfo> selectLoginInfoList(SysLoginInfo loginInfo) {
		return loginInfoMapper.selectLoginInfoList(loginInfo);
	}

	/**
	 * 批量删除系统登录日志
	 * 
	 * @param ids
	 *            需要删除的数据
	 * @return
	 */
	@Override
	@Transactional
	public int deleteLoginInfoByIds(String[] ids) {
		return loginInfoMapper.deleteLoginInfoByIds(ids);
	}

	/**
	 * 清空系统登录日志
	 */
	@Override
	public void cleanLoginInfo() {
		loginInfoMapper.cleanLoginInfo();
	}
}
