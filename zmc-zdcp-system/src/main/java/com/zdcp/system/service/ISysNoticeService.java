package com.zdcp.system.service;

import java.util.List;

import org.beetl.sql.core.engine.PageQuery;

import com.zdcp.system.bean.SysNotice;

/**
 * 公告 服务
 * 
 * @author yweijian
 * @date 2019年8月16日
 * @version 1.0
 */
public interface ISysNoticeService {
	/**
	 * 查询公告信息
	 * 
	 * @param noticeId
	 *            公告ID
	 * @return 公告信息
	 */
	public SysNotice selectNoticeById(Long noticeId);

	/**
	 * 查询公告列表
	 * 
	 * @param notice
	 *            公告信息
	 * @return 公告集合
	 */
	public List<SysNotice> selectNoticeList(PageQuery<SysNotice> query);

	/**
	 * 新增公告
	 * 
	 * @param notice
	 *            公告信息
	 * @return 结果
	 */
	public int insertNotice(SysNotice notice);

	/**
	 * 修改公告
	 * 
	 * @param notice
	 *            公告信息
	 * @return 结果
	 */
	public int updateNotice(SysNotice notice);

	/**
	 * 删除公告信息
	 * 
	 * @param ids
	 *            需要删除的数据ID
	 * @return 结果
	 */
	public int deleteNoticeByIds(String[] ids);
}
