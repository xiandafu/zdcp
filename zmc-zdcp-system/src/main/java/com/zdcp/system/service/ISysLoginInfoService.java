package com.zdcp.system.service;

import java.util.List;

import org.beetl.sql.core.engine.PageQuery;

import com.zdcp.system.bean.SysLoginInfo;

/**
 * 系统访问日志服务
 * @author yweijian
 * @date 2019年8月23日
 * @version 1.0
 */
public interface ISysLoginInfoService {
	/**
	 * 新增系统登录日志
	 * 
	 * @param loginInfo
	 *            访问日志对象
	 */
	public void insertLoginInfo(SysLoginInfo loginInfo);

	/**
	 * 分页查询系统登录日志集合
	 * 
	 * @param loginInfo
	 *            访问日志对象
	 * @return 登录记录集合
	 */
	public List<SysLoginInfo> selectLoginInfoList(PageQuery<SysLoginInfo> query);
	
	/**
	 * 查询系统登录日志集合
	 * 
	 * @param loginInfo
	 *            访问日志对象
	 * @return 登录记录集合
	 */
	public List<SysLoginInfo> selectLoginInfoList(SysLoginInfo loginInfo);

	/**
	 * 批量删除系统登录日志
	 * 
	 * @param ids
	 *            需要删除的数据
	 * @return
	 */
	public int deleteLoginInfoByIds(String[] ids);

	/**
	 * 清空系统登录日志
	 */
	public void cleanLoginInfo();
}
