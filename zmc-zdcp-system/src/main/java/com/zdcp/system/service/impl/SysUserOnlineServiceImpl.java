package com.zdcp.system.service.impl;

import java.util.Date;
import java.util.List;

import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zdcp.system.bean.SysUserOnline;
import com.zdcp.system.mapper.SysUserOnlineMapperDao;
import com.zdcp.system.service.ISysUserOnlineService;
import com.zdcp.util.DateUtils;
import com.zdcp.util.StringUtils;

/**
 * 在线用户 服务
 * 
 * @author yweijian
 * @date 2019年8月16日
 * @version 1.0
 */
@Service
public class SysUserOnlineServiceImpl implements ISysUserOnlineService {
	@Autowired
	private SysUserOnlineMapperDao userOnlineMapper;

	/**
	 * 通过会话序号查询信息
	 * 
	 * @param sessionId
	 *            会话ID
	 * @return 在线用户信息
	 */
	@Override
	public SysUserOnline selectOnlineById(String sessionId) {
		return userOnlineMapper.selectOnlineById(sessionId);
	}

	/**
	 * 通过会话序号删除信息
	 * 
	 * @param sessionId
	 *            会话ID
	 * @return 在线用户信息
	 */
	@Override
	@Transactional
	public void deleteOnlineById(String sessionId) {
		SysUserOnline userOnline = this.selectOnlineById(sessionId);
		if (StringUtils.isNotNull(userOnline)) {
			userOnlineMapper.deleteOnlineById(sessionId);
		}
	}

	/**
	 * 通过会话序号删除信息
	 * 
	 * @param sessions
	 *            会话ID集合
	 * @return 在线用户信息
	 */
	@Override
	@Transactional
	public void deleteOnlineByIds(List<String> sessionIds) {
		for (String sessionId : sessionIds) {
			SysUserOnline userOnline = this.selectOnlineById(sessionId);
			if (StringUtils.isNotNull(userOnline)) {
				userOnlineMapper.deleteOnlineById(sessionId);
			}
		}
	}

	/**
	 * 保存会话信息
	 * 
	 * @param online
	 *            会话信息
	 */
	@Override
	@Transactional
	public void insertOnline(SysUserOnline online) {
		userOnlineMapper.insert(online);
	}
	
	/**
	 * 更新会话信息
	 * 
	 * @param online
	 *            会话信息
	 */
	@Override
	@Transactional
	public void updateOnline(SysUserOnline online) {
		userOnlineMapper.updateOnline(online);
	}

	/**
	 * 查询会话集合
	 * 
	 * @param userOnline
	 *            在线用户
	 */
	@Override
	public List<SysUserOnline> selectUserOnlineList(PageQuery<SysUserOnline> query) {
		userOnlineMapper.selectUserOnlineList(query);
		return query.getList();
	}

	/**
	 * 强退用户
	 * 
	 * @param sessionId
	 *            会话ID
	 */
	@Override
	@Transactional
	public void forceLogout(String sessionId) {
		userOnlineMapper.deleteOnlineById(sessionId);
	}

	/**
	 * 查询会话集合
	 * 
	 * @param expiredDate
	 *            失效日期
	 */
	@Override
	public List<SysUserOnline> selectOnlineByExpired(Date expiredDate) {
		String lastAccessTime = DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD_HH_MM_SS, expiredDate);
		return userOnlineMapper.selectOnlineByExpired(lastAccessTime);
	}
}
