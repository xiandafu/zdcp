package com.zdcp.system.service.impl;

import java.util.Date;
import java.util.List;

import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zdcp.system.bean.SysNotice;
import com.zdcp.system.mapper.SysNoticeMapperDao;
import com.zdcp.system.service.ISysNoticeService;

/**
 * 公告 服务层实现
 * 
 * @author yweijian
 * @date 2019年8月16日
 * @version 1.0
 */
@Service
public class SysNoticeServiceImpl implements ISysNoticeService {
	@Autowired
	private SysNoticeMapperDao noticeMapper;

	/**
	 * 查询公告信息
	 * 
	 * @param noticeId
	 *            公告ID
	 * @return 公告信息
	 */
	@Override
	public SysNotice selectNoticeById(Long noticeId) {
		return noticeMapper.selectNoticeById(noticeId);
	}

	/**
	 * 查询公告列表
	 * 
	 * @param notice
	 *            公告信息
	 * @return 公告集合
	 */
	@Override
	public List<SysNotice> selectNoticeList(PageQuery<SysNotice> query) {
		noticeMapper.selectNoticeList(query);
		return query.getList();
	}

	/**
	 * 新增公告
	 * 
	 * @param notice
	 *            公告信息
	 * @return 结果
	 */
	@Override
	@Transactional
	public int insertNotice(SysNotice notice) {
		notice.setCreateTime(new Date());
		noticeMapper.insert(notice, true);
		return 1;
	}

	/**
	 * 修改公告
	 * 
	 * @param notice
	 *            公告信息
	 * @return 结果
	 */
	@Override
	@Transactional
	public int updateNotice(SysNotice notice) {
		notice.setUpdateTime(new Date());
		noticeMapper.updateTemplateById(notice);
		return 1;
	}

	/**
	 * 删除公告对象
	 * 
	 * @param ids
	 *            需要删除的数据ID
	 * @return 结果
	 */
	@Override
	@Transactional
	public int deleteNoticeByIds(String[] ids) {
		return noticeMapper.deleteNoticeByIds(ids);
	}
}
