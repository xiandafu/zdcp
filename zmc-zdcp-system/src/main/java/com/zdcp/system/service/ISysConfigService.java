package com.zdcp.system.service;

import java.util.List;

import org.beetl.sql.core.engine.PageQuery;

import com.zdcp.system.bean.SysConfig;

/**
 * 参数配置服务
 * 
 * @author yweijian
 * @date 2019年8月15日
 * @version 1.0
 */
public interface ISysConfigService {
	/**
	 * 查询参数配置信息
	 * 
	 * @param configId
	 *            参数配置ID
	 * @return 参数配置信息
	 */
	public SysConfig selectConfigById(Long configId);

	/**
	 * 根据键名查询参数配置信息
	 * 
	 * @param configKey
	 *            参数键名
	 * @return 参数键值
	 */
	public String selectConfigByKey(String configKey);

	/**
	 * 查询参数配置列表
	 * 
	 * @param config
	 *            参数配置信息
	 * @return 参数配置集合
	 */
	public List<SysConfig> selectConfigList(SysConfig config);
	/**
	 * 分页查询参数配置列表
	 * 
	 * @param config
	 *            参数配置信息
	 * @return 参数配置集合
	 */
	public List<SysConfig> selectConfigList(PageQuery<SysConfig> query);
	/**
	 * 新增参数配置
	 * 
	 * @param config
	 *            参数配置信息
	 * @return 结果
	 */
	public int insertConfig(SysConfig config);

	/**
	 * 修改参数配置
	 * 
	 * @param config
	 *            参数配置信息
	 * @return 结果
	 */
	public int updateConfig(SysConfig config);

	/**
	 * 批量删除参数配置信息
	 * 
	 * @param ids
	 *            需要删除的数据ID
	 * @return 结果
	 */
	public int deleteConfigByIds(String[] ids);

	/**
	 * 校验参数键名是否唯一
	 * 
	 * @param config
	 *            参数信息
	 * @return 结果
	 */
	public String checkConfigKeyUnique(SysConfig config);
}
