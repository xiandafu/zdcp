package com.zdcp.system.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zdcp.core.bean.Ztree;
import com.zdcp.core.exception.BusinessException;
import com.zdcp.system.constant.SysConstants;
import com.zdcp.system.bean.SysDictType;
import com.zdcp.system.mapper.SysDictDataMapperDao;
import com.zdcp.system.mapper.SysDictTypeMapperDao;
import com.zdcp.system.service.ISysDictTypeService;
import com.zdcp.util.StringUtils;

/**
 * 字典 业务层实现
 * 
 * @author yweijian
 * @date 2019年8月16日
 * @version 1.0
 */
@Service
public class SysDictTypeServiceImpl implements ISysDictTypeService {
	@Autowired
	private SysDictTypeMapperDao dictTypeMapper;

	@Autowired
	private SysDictDataMapperDao dictDataMapper;

	/**
	 * 根据条件分页查询字典类型
	 * 
	 * @param dictType
	 *            字典类型信息
	 * @return 字典类型集合信息
	 */
	@Override
	public List<SysDictType> selectDictTypeList(PageQuery<SysDictType> query) {
		dictTypeMapper.selectDictTypeList(query);
		return query.getList();
	}
	
	/**
	 * 根据条件查询字典类型
	 * 
	 * @param dictType
	 *            字典类型信息
	 * @return 字典类型集合信息
	 */
	@Override
	public List<SysDictType> selectDictTypeList(SysDictType dictType) {
		return dictTypeMapper.selectDictTypeList(dictType);
	}

	/**
	 * 根据所有字典类型
	 * 
	 * @return 字典类型集合信息
	 */
	@Override
	public List<SysDictType> selectDictTypeAll() {
		return dictTypeMapper.selectDictTypeAll();
	}

	/**
	 * 根据字典类型ID查询信息
	 * 
	 * @param dictId
	 *            字典类型ID
	 * @return 字典类型
	 */
	@Override
	public SysDictType selectDictTypeById(Long dictId) {
		return dictTypeMapper.selectDictTypeById(dictId);
	}

	/**
	 * 根据字典类型查询信息
	 * 
	 * @param dictType
	 *            字典类型
	 * @return 字典类型
	 */
	public SysDictType selectDictTypeByType(String dictType) {
		return dictTypeMapper.selectDictTypeByType(dictType);
	}

	/**
	 * 通过字典ID删除字典信息
	 * 
	 * @param dictId
	 *            字典ID
	 * @return 结果
	 */
	@Override
	@Transactional
	public int deleteDictTypeById(Long dictId) {
		return dictTypeMapper.deleteDictTypeById(dictId);
	}

	/**
	 * 批量删除字典类型
	 * 
	 * @param ids
	 *            需要删除的数据
	 * @return 结果
	 */
	@Override
	@Transactional
	public int deleteDictTypeByIds(String[] ids) throws BusinessException {
		for (String dictId : ids) {
			SysDictType dictType = selectDictTypeById(Long.parseLong(dictId));
			if (dictDataMapper.countDictDataByType(dictType.getDictType()) > 0) {
				throw new BusinessException(String.format("%1$s已分配,不能删除", dictType.getDictName()));
			}
		}

		return dictTypeMapper.deleteDictTypeByIds(ids);
	}

	/**
	 * 新增保存字典类型信息
	 * 
	 * @param dictType
	 *            字典类型信息
	 * @return 结果
	 */
	@Override
	@Transactional
	public int insertDictType(SysDictType dictType) {
		dictType.setCreateTime(new Date());
		dictTypeMapper.insert(dictType, true);
		return 1;
	}

	/**
	 * 修改保存字典类型信息
	 * 
	 * @param dictType
	 *            字典类型信息
	 * @return 结果
	 */
	@Override
	@Transactional
	public int updateDictType(SysDictType dictType) {
		SysDictType oldDict = dictTypeMapper.selectDictTypeById(dictType.getDictId());
		dictDataMapper.updateDictDataType(oldDict.getDictType(), dictType.getDictType());
		dictType.setUpdateTime(new Date());
		dictTypeMapper.updateTemplateById(dictType);
		return 1;
	}

	/**
	 * 校验字典类型称是否唯一
	 * 
	 * @param dict
	 *            字典类型
	 * @return 结果
	 */
	@Override
	public String checkDictTypeUnique(SysDictType dict) {
		Long dictId = StringUtils.isNull(dict.getDictId()) ? -1L : dict.getDictId();
		SysDictType dictType = dictTypeMapper.checkDictTypeUnique(dict.getDictType());
		if (StringUtils.isNotNull(dictType) && dictType.getDictId().longValue() != dictId.longValue()) {
			return SysConstants.NOT_UNIQUE;
		}
		return SysConstants.IS_UNIQUE;
	}

	/**
	 * 查询字典类型树
	 * 
	 * @param dictType
	 *            字典类型
	 * @return 所有字典类型
	 */
	public List<Ztree> selectDictTree(SysDictType dictType) {
		List<Ztree> ztrees = new ArrayList<Ztree>();
		List<SysDictType> dictList = dictTypeMapper.selectDictTypeList(dictType);
		for (SysDictType dict : dictList) {
			if (SysConstants.DICT_NORMAL.equals(dict.getStatus())) {
				Ztree ztree = new Ztree();
				ztree.setId(dict.getDictId());
				ztree.setName(transDictName(dict));
				ztree.setTitle(dict.getDictType());
				ztrees.add(ztree);
			}
		}
		return ztrees;
	}

	public String transDictName(SysDictType dictType) {
		StringBuffer sb = new StringBuffer();
		sb.append("(" + dictType.getDictName() + ")");
		sb.append("&nbsp;&nbsp;&nbsp;" + dictType.getDictType());
		return sb.toString();
	}
}
