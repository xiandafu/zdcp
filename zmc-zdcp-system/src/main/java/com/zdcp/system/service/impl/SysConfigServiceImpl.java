package com.zdcp.system.service.impl;

import java.util.Date;
import java.util.List;

import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zdcp.system.constant.SysConstants;
import com.zdcp.system.bean.SysConfig;
import com.zdcp.system.mapper.SysConfigMapperDao;
import com.zdcp.system.service.ISysConfigService;
import com.zdcp.util.StringUtils;

/**
 * 参数配置实现
 * @author yweijian
 * @date 2019年8月23日
 * @version 1.0
 */
@Service
public class SysConfigServiceImpl implements ISysConfigService {
	@Autowired
	private SysConfigMapperDao configMapper;

	/**
	 * 查询参数配置信息
	 * 
	 * @param configId
	 *            参数配置ID
	 * @return 参数配置信息
	 */
	@Override
	public SysConfig selectConfigById(Long configId) {
		SysConfig config = new SysConfig();
		config.setConfigId(configId);
		return configMapper.selectConfig(config);
	}

	/**
	 * 根据键名查询参数配置信息
	 * 
	 * @param configKey
	 *            参数key
	 * @return 参数键值
	 */
	@Override
	public String selectConfigByKey(String configKey) {
		SysConfig config = new SysConfig();
		config.setConfigKey(configKey);
		SysConfig retConfig = configMapper.selectConfig(config);
		return StringUtils.isNotNull(retConfig) ? retConfig.getConfigValue() : "";
	}

	/**
	 * 查询参数配置列表
	 * 
	 * @param config
	 *            参数配置信息
	 * @return 参数配置集合
	 */
	@Override
	public List<SysConfig> selectConfigList(SysConfig config) {
		return configMapper.selectConfigList(config);
	}
	
	/**
	 * 分页查询参数配置列表
	 * 
	 * @param config
	 *            参数配置信息
	 * @return 参数配置集合
	 */
	@Override
	public List<SysConfig> selectConfigList(PageQuery<SysConfig> query) {
		configMapper.selectConfigList(query);
		return query.getList();
	}

	/**
	 * 新增参数配置
	 * 
	 * @param config
	 *            参数配置信息
	 * @return 结果
	 */
	@Override
	@Transactional
	public int insertConfig(SysConfig config) {
		config.setCreateTime(new Date());
		configMapper.insert(config, true);
		return 1;
	}

	/**
	 * 修改参数配置
	 * 
	 * @param config
	 *            参数配置信息
	 * @return 结果
	 */
	@Override
	@Transactional
	public int updateConfig(SysConfig config) {
		config.setUpdateTime(new Date());
		configMapper.updateTemplateById(config);
		return 1;
	}

	/**
	 * 批量删除参数配置对象
	 * 
	 * @param ids
	 *            需要删除的数据ID
	 * @return 结果
	 */
	@Override
	@Transactional
	public int deleteConfigByIds(String[] ids) {
		return configMapper.deleteConfigByIds(ids);
	}

	/**
	 * 校验参数键名是否唯一
	 * 
	 * @param config
	 *            参数配置信息
	 * @return 结果
	 */
	@Override
	public String checkConfigKeyUnique(SysConfig config) {
		Long configId = StringUtils.isNull(config.getConfigId()) ? -1L : config.getConfigId();
		SysConfig info = configMapper.checkConfigKeyUnique(config.getConfigKey());
		if (StringUtils.isNotNull(info) && info.getConfigId().longValue() != configId.longValue()) {
			return SysConstants.NOT_UNIQUE;
		}
		return SysConstants.IS_UNIQUE;
	}
}
