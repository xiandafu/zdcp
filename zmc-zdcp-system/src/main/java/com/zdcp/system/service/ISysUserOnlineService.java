package com.zdcp.system.service;

import java.util.Date;
import java.util.List;

import org.beetl.sql.core.engine.PageQuery;

import com.zdcp.system.bean.SysUserOnline;

/**
 * 在线用户 服务
 * 
 * @author yweijian
 * @date 2019年8月16日
 * @version 1.0
 */
public interface ISysUserOnlineService {
	/**
	 * 通过会话序号查询信息
	 * 
	 * @param sessionId
	 *            会话ID
	 * @return 在线用户信息
	 */
	public SysUserOnline selectOnlineById(String sessionId);

	/**
	 * 通过会话序号删除信息
	 * 
	 * @param sessionId
	 *            会话ID
	 * @return 在线用户信息
	 */
	public void deleteOnlineById(String sessionId);

	/**
	 * 通过会话序号删除信息
	 * 
	 * @param sessionIds
	 *            会话ID集合
	 * @return 在线用户信息
	 */
	public void deleteOnlineByIds(List<String> sessionIds);

	/**
	 * 保存会话信息
	 * 
	 * @param online
	 *            会话信息
	 */
	public void insertOnline(SysUserOnline online);
	
	/**
	 * 更新会话信息
	 * 
	 * @param online
	 *            会话信息
	 */
	public void updateOnline(SysUserOnline online);

	/**
	 * 查询会话集合
	 * 
	 * @param userOnline
	 *            分页参数
	 * @return 会话集合
	 */
	public List<SysUserOnline> selectUserOnlineList(PageQuery<SysUserOnline> query);

	/**
	 * 强退用户
	 * 
	 * @param sessionId
	 *            会话ID
	 */
	public void forceLogout(String sessionId);

	/**
	 * 查询会话集合
	 * 
	 * @param expiredDate
	 *            有效期
	 * @return 会话集合
	 */
	public List<SysUserOnline> selectOnlineByExpired(Date expiredDate);
}
