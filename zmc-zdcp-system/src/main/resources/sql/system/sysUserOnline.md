sys_user_online
===
	@pageTag(){
	session_id, login_name, dept_name, ipaddr, login_location, browser, os, status, start_timestamp, last_access_time, expire_time 
	@}
	from sys_user_online

selectOnlineById
===
	select #use('sys_user_online')# WHERE session_id = #sessionId#

deleteOnlineById
===
	delete from sys_user_online WHERE session_id = #sessionId#

selectUserOnlineList
===
	select #use('sys_user_online')# WHERE 1=1
	@if(!isEmpty(ipaddr)){
		AND ipaddr like concat('%', #ipaddr#, '%')
	@}
	@if(!isEmpty(loginName)){
		AND login_name like concat('%', #loginName#, '%')
	@}

selectOnlineByExpired
===
	select #use('sys_user_online')# WHERE last_access_time <= #lastAccessTime# ORDER BY last_access_time ASC
	
updateOnline
===
	update sys_user_online set status=#status# WHERE session_id = #sessionId#
