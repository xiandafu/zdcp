sys_oper_log
===
	@pageTag(){
	oper_id, title, business_type, method, operator_type, oper_name, dept_name, oper_url, oper_ip, oper_location, oper_param, status, error_msg, oper_time
    @}
    from sys_oper_log

selectOperLogList
===
	select #use('sys_oper_log')# where 1=1
	@if(!isEmpty(title)){
		AND title like concat('%', #title#, '%')
	@}
	@if(!isEmpty(businessType)){
		AND business_type = #businessType#
	@}
	@if(!isEmpty(businessTypes)){
		AND business_type in(#join(businessTypes)#)
	@}
	@if(!isEmpty(status)){
		AND status = #status#
	@}
	@if(!isEmpty(operName)){
		AND oper_name like concat('%', #operName#, '%')
	@}
	@if(!isEmpty(params.beginTime)){
		AND date_format(u.create_time,'%y%m%d') >= date_format(#params['beginTime']#,'%y%m%d')
	@}
	@if(!isEmpty(params.endTime)){
		AND date_format(u.create_time,'%y%m%d') <= date_format(#params['endTime']#,'%y%m%d')
	@}

deleteOperLogByIds
===
	delete from sys_oper_log where oper_id in(#join(ids)#)

selectOperLogById
===
	select #use('sys_oper_log')# where oper_id = #operId#

cleanOperLog
===
	truncate table sys_oper_log