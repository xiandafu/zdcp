sys_dict_type
===
	@pageTag(){
	dict_id, dict_name, dict_type, status, create_by, create_time, remark 
	@}
	from sys_dict_type

selectDictTypeList
===
	select #use('sys_dict_type')# where 1=1
	@if(!isEmpty(dictName)){
		AND dict_name like concat('%', #dictName#, '%')
	@}
	@if(!isEmpty(status)){
		AND status = #status#
	@}
	@if(!isEmpty(dictType)){
		AND dict_type like concat('%', #dictType#, '%')
	@}
	@if(!isEmpty(params.beginTime)){
		AND date_format(u.create_time,'%y%m%d') >= date_format(#params['beginTime']#,'%y%m%d')
	@}
	@if(!isEmpty(params.endTime)){
		AND date_format(u.create_time,'%y%m%d') <= date_format(#params['endTime']#,'%y%m%d')
	@}

selectDictTypeAll
===
	select #use('sys_dict_type')# 

selectDictTypeById
===
	select #use('sys_dict_type')#  where dict_id = #dictId#

selectDictTypeByType
===
	select #use('sys_dict_type')#  where dict_type = #dictType#

checkDictTypeUnique
===
	select #use('sys_dict_type')#  where dict_type = #dictType#

deleteDictTypeById
===
	delete from sys_dict_type where dict_id = #dictId#

deleteDictTypeByIds
===
	delete from sys_dict_type where dict_id in(#join(ids)#)
