sys_dict_data
===
	@pageTag(){
	dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, remark 
	@}
	from sys_dict_data

selectDictDataList
===
	select #use('sys_dict_data')# WHERE 1=1
	@if(!isEmpty(dictType)){
		AND dict_type = #dictType#
	@}
	@if(!isEmpty(dictLabel)){
		AND dict_label like concat('%', #dictLabel#, '%')
	@}
	@if(!isEmpty(status)){
		AND status = #status#
	@}

selectDictDataByType
===
	select #use('sys_dict_data')# where status = '0' and dict_type = #dictType# order by dict_sort asc

selectDictLabel
===
	select dict_label from sys_dict_data
		where dict_type = #dictType# and dict_value = #dictValue#

selectDictDataById
===
	select #use('sys_dict_data')# where dict_code = #dictCode#

countDictDataByType
===
	select count(1) from sys_dict_data where dict_type=#dictType#

deleteDictDataById
===
	delete from sys_dict_data where dict_code = #dictCode#

deleteDictDataByIds
===
	delete from sys_dict_data where dict_code in(#join(ids)#)

updateDictDataType
===
	update sys_dict_data set dict_type = #newDictType# where dict_type = #oldDictType#

