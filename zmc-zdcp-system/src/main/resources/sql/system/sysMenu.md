sys_menu
===
	@pageTag(){
	menu_id, menu_name, parent_id, order_num, url, target, menu_type, visible, ifnull(perms,'') as perms, icon, create_by, create_time 
	@}
	from sys_menu

selectMenusByUserId
===
	select distinct m.menu_id, m.parent_id, m.menu_name, m.url, m.visible, ifnull(m.perms,'') as perms, m.target, m.menu_type, m.icon, m.order_num, m.create_time
		from sys_menu m
			 left join sys_role_menu rm on m.menu_id = rm.menu_id
			 left join sys_user_role ur on rm.role_id = ur.role_id
			 LEFT JOIN sys_role ro on ur.role_id = ro.role_id
		where ur.user_id = #{userId} and m.menu_type in ('M', 'C') and m.visible = 0  AND ro.status = 0
		order by m.parent_id, m.order_num

selectMenuNormalAll
===
	select distinct m.menu_id, m.parent_id, m.menu_name, m.url, m.visible, ifnull(m.perms,'') as perms, m.target, m.menu_type, m.icon, m.order_num, m.create_time
		from sys_menu m
		where m.menu_type in ('M', 'C') and m.visible = 0
		order by m.parent_id, m.order_num

selectMenuAll
===
	select #use('sys_menu')# order by parent_id, order_num

selectMenuAllByUserId
===
	select distinct m.menu_id, m.parent_id, m.menu_name, m.url, m.visible, ifnull(m.perms,'') as perms, m.target, m.menu_type, m.icon, m.order_num, m.create_time
		from sys_menu m
			 left join sys_role_menu rm on m.menu_id = rm.menu_id
			 left join sys_user_role ur on rm.role_id = ur.role_id
			 LEFT JOIN sys_role ro on ur.role_id = ro.role_id
		where ur.user_id = #userId#
		order by m.parent_id, m.order_num

selectPermsByUserId
===
	select distinct m.perms
		from sys_menu m
			 left join sys_role_menu rm on m.menu_id = rm.menu_id
			 left join sys_user_role ur on rm.role_id = ur.role_id
		where ur.user_id = #userId#

selectMenuTree
===
	select concat(m.menu_id, ifnull(m.perms,'')) as perms
		from sys_menu m
			left join sys_role_menu rm on m.menu_id = rm.menu_id
		where rm.role_id = #roleId#
		order by m.parent_id, m.order_num

selectMenuList
===
	select #use('sys_menu')# where 1=1
	@if(!isEmpty(menuName)){
		AND menu_name like concat('%', #menuName#, '%')
	@}
	@if(!isEmpty(visible)){
		AND visible = #visible#
	@}
	order by parent_id, order_num

selectMenuListByUserId
===
	select distinct m.menu_id, m.parent_id, m.menu_name, m.url, m.visible, ifnull(m.perms,'') as perms, m.target, m.menu_type, m.icon, m.order_num, m.create_time
		from sys_menu m
		left join sys_role_menu rm on m.menu_id = rm.menu_id
		left join sys_user_role ur on rm.role_id = ur.role_id
		LEFT JOIN sys_role ro on ur.role_id = ro.role_id
		where ur.user_id = #params['userId']#
	@if(!isEmpty(menuName)){
		AND menu_name like concat('%', #menuName#, '%')
	@}
	@if(!isEmpty(visible)){
		AND visible = #visible#
	@}
	order by parent_id, order_num

deleteMenuById
===
	delete from sys_menu where menu_id = #menuId# or parent_id = #menuId#

selectMenuById
===
	SELECT t.menu_id, t.parent_id, t.menu_name, t.order_num, t.url, t.target, t.menu_type, t.visible, t.perms, t.icon, t.remark,
			(SELECT menu_name FROM sys_menu WHERE menu_id = t.parent_id) parent_name
		FROM sys_menu t
		where t.menu_id = #menuId#

selectCountMenuByParentId
===
	select count(1) from sys_menu where parent_id=#menuId#

checkMenuNameUnique
===
	select #use('sys_menu')# where menu_name=#menuName# and parent_id = #parentId#
