sys_post
===
	@pageTag(){
	post_id, post_code, post_name, post_sort, status, create_by, create_time, remark 
	@}
	from sys_post

selectPostList
===
	select #use('sys_post')# WHERE 1=1
	@if(!isEmpty(postCode)){
		AND post_code like concat('%', #postCode#, '%')
	@}
	@if(!isEmpty(status)){
		AND status = #status#
	@}
	@if(!isEmpty(postName)){
		AND post_name like concat('%', #postName#, '%')
	@}

selectPostAll
===
	select #use('sys_post')#

selectPostsByUserId
===
	SELECT p.post_id, p.post_name, p.post_code
		FROM sys_user u
			 LEFT JOIN sys_user_post up ON u.user_id = up.user_id
			 LEFT JOIN sys_post p ON up.post_id = p.post_id
		WHERE up.user_id = #userId#

selectPostById
===
	select #use('sys_post')# WHERE post_id = #postId#

checkPostNameUnique
===
	select #use('sys_post')# WHERE post_name=#postName#

checkPostCodeUnique
===
	select #use('sys_post')# WHERE post_code=#postCode#

deletePostByIds
===
	delete from sys_post WHERE post_id in(#join(ids)#)
