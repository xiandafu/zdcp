sys_user
===
	@pageTag(){
	u.user_id, u.dept_id, u.login_name, u.user_name, u.email, u.avatar, u.phonenumber, u.sex, u.password, 
		u.salt, u.status, u.del_flag, u.login_ip, u.login_date, u.create_time, u.remark,
		d.dept_name, d.leader
	@}
	from sys_user u
		 left join sys_dept d on u.dept_id = d.dept_id
		 left join sys_user_role ur on u.user_id = ur.user_id
		 left join sys_role r on r.role_id = ur.role_id

selectUserList
===
	select 
	@pageTag(){
	u.user_id, u.dept_id, u.login_name, u.user_name, u.email, u.avatar, u.phonenumber, u.password, u.sex, 
	u.salt, u.status, u.del_flag, u.login_ip, u.login_date, u.create_by, u.create_time, u.remark, d.dept_name, d.leader 
	@}
	FROM sys_user u left join sys_dept d on u.dept_id = d.dept_id
		WHERE u.del_flag != '2' 
	@if(!isEmpty(loginName)){
		AND u.login_name like concat('%', #loginName#, '%')
    @}
	@if(!isEmpty(status)){
		AND u.status = #status#
    @}
	@if(!isEmpty(phonenumber)){
		AND u.phonenumber like concat('%', #phonenumber#, '%')
	@}
	@if(!isEmpty(params.beginTime)){
		AND date_format(u.create_time,'%y%m%d') >= date_format(#params['beginTime']#,'%y%m%d')
	@}
	@if(!isEmpty(params.endTime)){
		AND date_format(u.create_time,'%y%m%d') <= date_format(#params['endTime']#,'%y%m%d')
	@}
	@if(!isEmpty(deptId)){
		AND (u.dept_id = #deptId# OR u.dept_id IN ( SELECT t.dept_id FROM sys_dept t WHERE FIND_IN_SET (#deptId#,ancestors) ))
	@}
	#text(params['dataScope'])#

selectAllocatedList
===
	select #use('sys_user')# WHERE u.del_flag = '0' and r.role_id = #roleId#
	@if(!isEmpty(loginName)){
		AND u.login_name like concat('%', #loginName#, '%')
	@}
	@if(!isEmpty(phonenumber)){
		AND u.phonenumber like concat('%', #phonenumber#, '%')
	@}
	#text(params['dataScope'])#

selectUnallocatedList
===
	select #use('sys_user')# WHERE u.del_flag != '2' 
		AND (r.role_id != #roleId# or r.role_id IS NULL)
	    AND u.user_id not in (select u.user_id from sys_user u inner join sys_user_role ur on u.user_id = ur.user_id and ur.role_id = #roleId#)
	@if(!isEmpty(loginName)){
		AND u.login_name like concat('%', #loginName#, '%')
	@}
	@if(!isEmpty(phonenumber)){
		AND u.phonenumber like concat('%', #phonenumber#, '%')
	@}
	#text(params['dataScope'])#

selectUserByLoginName
===
	select #use('sys_user')# WHERE u.del_flag != '2' and u.login_name = #userName#

selectUserByPhoneNumber
===
	select #use('sys_user')# WHERE u.del_flag != '2' and u.phonenumber = #phonenumber#

selectUserByEmail
===
	select #use('sys_user')# WHERE u.del_flag != '2' and u.email = #email#

checkLoginNameUnique
===
	select count(1) from sys_user WHERE del_flag != '2' and login_name=#loginName#

checkPhoneUnique
===
	select user_id, phonenumber from sys_user WHERE del_flag != '2' and phonenumber=#phonenumber#

checkEmailUnique
===
	select user_id, email from sys_user WHERE del_flag != '2' and email=#email#

selectUserById
===
	select #use('sys_user')# WHERE u.user_id = #userId#

deleteUserById
===
	update sys_user set del_flag = '2' WHERE user_id = #userId#

deleteUserByIds
===
	update sys_user set del_flag = '2' WHERE user_id in(#join(ids)#)
