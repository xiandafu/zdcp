sys_config
===
	@pageTag(){
	config_id, config_name, config_key, config_value, config_type, create_by, create_time, update_by, update_time, remark 
	@}
	from sys_config 
		
selectConfig
===
	select #use('sys_config')# WHERE 1=1
	@if(!isEmpty(configId)){
		AND config_id = #configId#
	@}
	@if(!isEmpty(configKey)){
		AND config_key = #configKey#
	@}

selectConfigList
===
	select #use('sys_config')# WHERE 1=1
	@if(!isEmpty(configName)){
		AND config_name like concat('%', #configName#, '%')
	@}
	@if(!isEmpty(configType)){
		AND config_type = #configType#
	@}
	@if(!isEmpty(configKey)){
		AND config_key like concat('%', #configKey#, '%')
	@}
	@if(!isEmpty(params.beginTime)){
		AND date_format(create_time,'%y%m%d') >= date_format(#params['beginTime']#,'%y%m%d')
	@}
	@if(!isEmpty(params.endTime)){
		AND date_format(create_time,'%y%m%d') <= date_format(#params['endTime']#,'%y%m%d')
	@}

checkConfigKeyUnique
===
	select #use('sys_config')# WHERE config_key = #configKey#

deleteConfigByIds
===
	 delete from sys_config WHERE config_id in(#join(ids)#)
