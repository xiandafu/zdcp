deleteRoleDeptByRoleId
===
	delete from sys_role_dept where role_id=#roleId#

selectCountRoleDeptByDeptId
===
	select count(1) from sys_role_dept where dept_id=#deptId#

deleteRoleDept
===
	delete from sys_role_dept where role_id in(#join(ids)#)

batchRoleDept
===
	insert into sys_role_dept(role_id, dept_id) values
	@for(item in roleDeptList){
		(#item.roleId#,#item.deptId#) #text(itemLP.last ? "" : "," )#
	@}