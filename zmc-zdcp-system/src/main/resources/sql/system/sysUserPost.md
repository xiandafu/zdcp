deleteUserPostByUserId
===
	delete from sys_user_post where user_id=#userId#

countUserPostById
===
	 select count(1) from sys_user_post where post_id=#postId#

deleteUserPost
===
	delete from sys_user_post where user_id in(#join(ids)#)

batchUserPost
===
	insert into sys_user_post(user_id, post_id) values
	@for(item in userPostList){
		(#item.userId#,#item.postId#) #text(itemLP.last ? "" : "," )#
	@}