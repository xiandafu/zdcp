sys_dept
===
	@pageTag(){
	d.dept_id, d.parent_id, d.ancestors, d.dept_name, d.order_num, d.leader, d.phone, d.email, d.status, d.del_flag, d.create_by, d.create_time 
    @}
    from sys_dept d
        
selectRoleDeptTree
===
	select concat(d.dept_id, d.dept_name) as dept_name
		from sys_dept d
			left join sys_role_dept rd on d.dept_id = rd.dept_id
		where d.del_flag = '0' and rd.role_id = #roleId#
		order by d.parent_id, d.order_num
		
selectDeptList
===
	select #use('sys_dept')# where d.del_flag = '0'
	@if(!isEmpty(parentId)){
		AND parent_id = #parentId#
	@}
	@if(!isEmpty(deptName)){
		AND dept_name like concat('%', #deptName#, '%')
	@}
	@if(!isEmpty(status)){
		AND status = #status#
	@}
	#text(params['dataScope'])#
	order by d.parent_id, d.order_num

checkDeptExistUser
===
	select count(1) from sys_user where dept_id = #deptId# and del_flag = '0'
	
selectDeptCount
===
	select count(1) from sys_dept where del_flag = '0'
	@if(!isEmpty(deptId)){
		and dept_id = #deptId#
	@}
	@if(!isEmpty(parentId)){
		and parent_id = #parentId#
	@}
	
checkDeptNameUnique
===
	select #use('sys_dept')# where dept_name=#deptName# and parent_id = #parentId#

selectDeptById
===
	select d.dept_id, d.parent_id, d.ancestors, d.dept_name, d.order_num, d.leader, d.phone, d.email, d.status,
	(select dept_name from sys_dept where dept_id = d.parent_id) parent_name
		from sys_dept d
		where d.dept_id = #deptId#

selectChildrenDeptById
===
	select #use('sys_dept')# where find_in_set(#deptId#, ancestors)

updateDeptChildren
===
	 update sys_dept set ancestors = #depts[0].ancestors# where dept_id in(
	 @for(item in depts){
	 	#item.deptId# #text(itemLP.last ? ")" : "," )#
	 @}
	 
deleteDeptById
===
	update sys_dept set del_flag = '2' where dept_id = #deptId#

updateDeptStatus
===
	update sys_dept set
	@if(!isEmpty(status)){
		status = #status#,
	@}
	@if(!isEmpty(updateBy)){
		update_by = #updateBy#,
	@}
	update_time = sysdate()
	where dept_id in (#text(ancestors)#)
