sys_notice
===
	@pageTag(){
	notice_id, notice_title, notice_type, notice_content, status, create_by, create_time, update_by, update_time, remark 
	@}
	from sys_notice

selectNoticeById
===
	select #use('sys_notice')# where notice_id = #noticeId#

selectNoticeList
===
	select #use('sys_notice')# where 1=1
	@if(!isEmpty(noticeTitle)){
		AND notice_title like concat('%', #noticeTitle#, '%')
	@}
	@if(!isEmpty(noticeType)){
		AND notice_type = #noticeType#
	@}
	@if(!isEmpty(createBy)){
		AND create_by like concat('%', #createBy#, '%')
	@}

deleteNoticeByIds
===
	delete from sys_notice where notice_id in(#join(ids)#)
	
