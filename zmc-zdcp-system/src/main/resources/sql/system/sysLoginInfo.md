selectLoginInfoList
===
	select
	@pageTag(){ 
	info_id,login_name,ipaddr,login_location,browser,os,status,msg,login_time 
	@}
	from sys_login_info where 1=1
	@if(!isEmpty(ipaddr)){
		AND ipaddr like concat('%', #ipaddr#, '%')
	@}
	@if(!isEmpty(status)){
		AND status = #status#
	@}
	@if(!isEmpty(loginName)){
		AND login_name like concat('%', #loginName#, '%')
	@}
	@if(!isEmpty(params.beginTime)){
		AND date_format(u.create_time,'%y%m%d') >= date_format(#params['beginTime']#,'%y%m%d')
	@}
	@if(!isEmpty(params.endTime)){
		AND date_format(u.create_time,'%y%m%d') <= date_format(#params['endTime']#,'%y%m%d')
	@}

deleteLoginInfoByIds
===
	delete from sys_login_info where info_id in(#join(ids)#)

cleanLoginInfo
===
	truncate table sys_login_info
