sys_role_unit
===
	@pageTag(){
	distinct r.role_id, r.role_name, r.role_key, r.role_sort, r.data_scope,
            r.status, r.del_flag, r.create_time, r.remark 
    @}
    from sys_role r
        left join sys_user_role ur on ur.role_id = r.role_id
        left join sys_user u on u.user_id = ur.user_id
        left join sys_dept d on u.dept_id = d.dept_id	

sys_role
===
	@pageTag(){
	r.role_id, r.role_name, r.role_key, r.role_sort, r.data_scope, r.status, r.del_flag, r.create_time, r.remark 
    @}
    from sys_role r
        
selectRoleList
=== 
	select #use('sys_role_unit')# where r.del_flag != '2'
	@if(!isEmpty(roleName)){
		AND r.role_name like concat('%', #roleName#, '%')
	@}
	@if(!isEmpty(status)){
		AND status = #status#
	@}
	@if(!isEmpty(roleKey)){
		AND r.role_key like concat('%', #roleKey#, '%')
	@}
	@if(!isEmpty(dataScope)){
		AND r.data_scope = #dataScope#
	@}
	@if(!isEmpty(status)){
		AND status = #status#
	@}
	@if(!isEmpty(params.beginTime)){
		AND date_format(create_time,'%y%m%d') >= date_format(#params['beginTime']#,'%y%m%d')
	@}
	@if(!isEmpty(params.endTime)){
		AND date_format(create_time,'%y%m%d') <= date_format(#params['endTime']#,'%y%m%d')
	@}
	#text(params['dataScope'])#

selectRolesByUserId
===
	select #use('sys_role_unit')# WHERE r.del_flag != '2' and ur.user_id = #userId#

selectRoleById
===
	select #use('sys_role')# WHERE r.del_flag != '2' and r.role_id = #roleId#

checkRoleNameUnique
===
	select #use('sys_role')# WHERE r.del_flag != '2' and r.role_name = #roleName#

checkRoleKeyUnique
===
	select #use('sys_role')# WHERE r.del_flag != '2' and r.role_key = #roleKey#

deleteRoleById
===
	delete from sys_role WHERE role_id = #roleId#

deleteRoleByIds
===
	update sys_role set del_flag = '2' WHERE role_id in(#join(ids)#)
