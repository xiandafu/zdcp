deleteUserRoleByUserId
===
	delete from sys_user_role where user_id=#userId#

countUserRoleByRoleId
===
	select count(1) from sys_user_role where role_id=#roleId#

deleteUserRole
===
	delete from sys_user_role where user_id in(#join(ids)#)

batchUserRole
===
	insert into sys_user_role(user_id, role_id) values
	@for(item in userRoleList){
		(#item.userId#,#item.roleId#) #text(itemLP.last ? "" : "," )#
	@}

deleteUserRoleInfo
===
	delete from sys_user_role where user_id=#userId# and role_id=#roleId#

deleteUserRoleInfos
===
	delete from sys_user_role where role_id=#roleId# and user_id in(#join(userIds)#)

