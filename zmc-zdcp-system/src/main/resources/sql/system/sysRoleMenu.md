deleteRoleMenuByRoleId
===
	delete from sys_role_menu where role_id=#roleId#

selectCountRoleMenuByMenuId
===
	select count(1) from sys_role_menu where menu_id=#menuId#

deleteRoleMenu
===
	delete from sys_role_menu where role_id in(#join(ids)#)

batchRoleMenu
===
	insert into sys_role_menu(role_id, menu_id) values
	@for(item in roleMenuList){
		(#item.roleId#,#item.menuId#) #text(itemLP.last ? "" : "," )#
	@}