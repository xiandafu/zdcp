# zdcp

#### 介绍
ZDCP后台管理框架

在码云平台上有很多优秀的开源后台管理系统，看了很多优秀的开源项目结合朋友刚成立创业公司的需求。于是利用空闲休息时间帮忙写一套后台系统，于是有了这个管理系统。
后续根据朋友的需求以及个人需求，对系统会陆续更新一些实用功能。

####友情鸣谢

感谢“若依-RuoYi”这个优秀的开源项目，该平台代码基本参照若依后台管理系统改造。（别问为什么，为了赶时间！）

感谢“闲大赋-Beetlsql”这个优秀的开源项目，该平台数据操作选择BeetSql模板引擎。（别问为什么，朋友特殊要求！）

#### 技术

    核心框架：Spring Boot。
    安全框架：Apache Shiro。
    模板引擎：Thymeleaf。
    持久层框架：Beetl。
    工具类：Fastjson。


#### 安装教程

1. 创建数据库并导入文件在doc文件夹下的zdcp-201908291940.sql
    win+r cmd -> 
    mysql -P 3306 -h 127.0.0.1 -u root -p123456 --default-character-set=utf8 zdcp < C:\Users\Administrator\Desktop\zdcp-201908291940.sql
2. 修改application.properties文件配置属性
3. 运行ZdcpApplication.java的main方法

#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)