package com.zdcp.core.enums;

/**
 * 操作人类别
 * 
 * @author yweijian
 * @date 2019年8月18日
 * @version 1.0
 */
public enum OperatorType {

	/**
	 * 后台端
	 */
	MANAGE,

	/**
	 * 移动端
	 */
	MOBILE,
	/**
	 * 微信
	 */
	WEIXIN,
	/**
	 * QQ
	 */
	QQ,
	/**
	 * 微博
	 */
	WEIBO,
	/**
	 * 其它
	 */
	OTHER
}
