package com.zdcp.core.enums;

/**
 * Token类型
 * 
 * @author yweijian
 * @date 2019年8月18日
 * @version 1.0
 */
public enum TokenType {

	/** 创建 **/
	CREATE,

	/** 销毁 **/
	DESTROY
}
