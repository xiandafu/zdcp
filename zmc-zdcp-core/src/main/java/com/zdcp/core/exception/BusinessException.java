package com.zdcp.core.exception;

/**
 * 业务异常
 * 
 * @author yweijian
 * @date 2019年8月18日
 * @version 1.0
 */
public class BusinessException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	protected final String message;

	public BusinessException(String message) {
		this.message = message;
	}

	public BusinessException(String message, Throwable e) {
		super(message, e);
		this.message = message;
	}

	@Override
	public String getMessage() {
		return message;
	}
}
