package com.zdcp.core.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import com.zaxxer.hikari.HikariDataSource;

/**
 * 
 * @author yweijian
 * @date 2019年8月18日
 * @version 1.0
 */
@Configuration
public class DataSourceConfig {	
	
	@Bean(value="baseDataSource")
	public DataSource datasource(Environment env) {
		HikariDataSource hikariDataSource = new HikariDataSource();
		hikariDataSource.setJdbcUrl(env.getProperty("spring.datasource.baseDataSource.url"));
		hikariDataSource.setUsername(env.getProperty("spring.datasource.baseDataSource.username"));
		hikariDataSource.setPassword(env.getProperty("spring.datasource.baseDataSource.password"));
		hikariDataSource.setDriverClassName(env.getProperty("spring.datasource.baseDataSource.driver-class-name"));
		return hikariDataSource;
	}
}



